///************************************************************************************//**
//* \file         inc/adc.c
//* \brief        Power_sys application source file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//#include "FreeRTOS.h"
//#include "MDR32Fx.h"
//#include "task.h"
//
//#include "MDR32F9Qx_config.h"
//#include "MDR32F9Qx_comp.h"
//#include "MDR32F9Qx_rst_clk.h"
//#include "compara.h"
//#include "types.h"
//#include "trace.h"
//
//MODULE_NAME("compara.c");
//
//static COMP_InitTypeDef COMP_InitStructure;
//static COMP_CVRefInitTypeDef COMP_CVRefInitStructure;
//static compara_levels_t level;
///****************************************************************************************
//* Exported variables
//****************************************************************************************/
///****************************************************************************************
//* Function prototypes
//****************************************************************************************/
//static int compara_init_hardware(void);
//
///************************************************************************************//**
//** \brief     Initializes the Comparator.
//** \return    0 if all ok.
//**
//****************************************************************************************/
//int compara_init(void)
//{
//    int result = 0;
//    TRACE_INFO("Initialize COMP");
//    level = COMP_LVL_STANDARD;
//    if ((result = compara_init_hardware())) {
//        TRACE_FATAL("Fail init COMP hardware");
//        return -1;
//    }
//    return 0;
//}
//
///************************************************************************************//**
//** \brief     Work and get untriggered comparator status.
//** \return    0 if plus input greater minus input.
//**
//****************************************************************************************/
//uint32_t compara_get_untriggered_status(void)
//{
//      return COMP_GetFlagStatus(COMP_STATUS_FLAG_AS);
//}
//
//int compara_down_level(void)
//{
//    TRACE_INFO("Comp level down");
//    COMP_Cmd(DISABLE);
//    COMP_CVRefInitStructure.COMP_CVRefScale = COMP_CVREF_SCALE_13_div_32;
//    COMP_CVRefInit(&COMP_CVRefInitStructure);
//    COMP_Cmd(ENABLE);
//    level = COMP_LVL_DOWN;
//    return 0;
//}
//
//int compara_up_level(void)
//{
//    TRACE_INFO("Comp level restore");
//    COMP_Cmd(DISABLE);
//    COMP_CVRefInitStructure.COMP_CVRefScale = COMP_CVREF_SCALE_12_div_32;
//    COMP_CVRefInit(&COMP_CVRefInitStructure);
//    COMP_Cmd(ENABLE);
//    level = COMP_LVL_STANDARD;
//    return 0;
//}
//
//int compara_is_lvl_standard(void)
//{
//    return ((level == COMP_LVL_STANDARD) ? 1 : 0);
//}
///************************************************************************************//**
//** \brief     Initializes the Comparator hardware.
//** \return    0 if all ok.
//**
//****************************************************************************************/
//static int compara_init_hardware(void)
//{
//    volatile uint32_t repeats = 80000;
//
//    /* Enables the HSI clock for COMP control */
//    RST_CLK_PCLKcmd(RST_CLK_PCLK_COMP,ENABLE);
//
//    /* Fills each COMP_InitStructure member with its default value */
//    COMP_StructInit(&COMP_InitStructure);
//
//    /* Initialize COMP_InitStructure */
//    COMP_InitStructure.COMP_PlusInputSource = COMP_PlusInput_CVREF;
//    COMP_InitStructure.COMP_MinusInputSource = COMP_MinusInput_IVREF; //
//#ifdef USE_INVERTION
//    COMP_InitStructure.COMP_OutInversion = COMP_OUT_INV_Enable;
//#endif
//
//    /* Configure COMP */
//    COMP_Init(&COMP_InitStructure);
//
//    /* Fills each COMP_CVRefInitStructure member with its default value */
//    COMP_CVRefStructInit(&COMP_CVRefInitStructure);
//
//    /* Initialize COMP_CVRefInitStructure */
//    COMP_CVRefInitStructure.COMP_CVRefSource = COMP_CVREF_SOURCE_AVdd;
//    COMP_CVRefInitStructure.COMP_CVRefScale = COMP_CVREF_SCALE_12_div_32;
//
//    /* Configure COMP CVRef */
//    COMP_CVRefInit(&COMP_CVRefInitStructure);
//
//    /* Enables COMP peripheral */
//    COMP_Cmd(ENABLE);
//       /* Check READY flag */
//    while((COMP_GetCfgFlagStatus(COMP_CFG_FLAG_READY) != SET)
//            && --repeats) { }
//    if (repeats <= 0) {
//        return -1;
//    }
//
//    /* Enables COMP CVRef */
//    COMP_CVRefCmd(ENABLE);
//
//    return 0;
//}
//
//
///*********************************** end of compara.c **************************************/
