///************************************************************************************//**
//* \file         inc/states.c
//* \brief        Power_sys application module file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//#include "FreeRTOSConfig.h"
//#include "MDR32F9Qx_config.h"
//#include "MDR32F9Qx_power.h"
//#include "FreeRTOS.h"
//#include "MDR32Fx.h"
//#include "portmacro.h"
//
//#include "states.h"
//#include "pins.h"
//#include "mp2731.h"
//#include "task.h"
//#include "adc.h"
//#include "ups_commands.h"
//#include "compara.h"
//#include "trace.h"
//
//#ifdef TEST_ENA
//#include "module_test.h"
//#endif
//
//MODULE_NAME("states.c");
//
//
//typedef enum {
//    HOST_NORMAL         = 0x0,
//    HOST_IN_BAT_CHARG   = 0x01,
//    HOST_BAT            = 0x02,
//    HOST_ERROR          = 0x03,
//} host_state_t;
//
//typedef enum {
//    ST_INIT = 0,
//    ST_TRANSPORT = 1,
//    ST_MONITOR = 2,
//    ST_SRC_ADAPTOR =3,
//    ST_SRC_BATTERY = 4,
//    ST_POWER_OFF = 5,
//    ST_TEST_END = 6,
//    ST_SHUTDOWN = 7,
//    ST_MAX = 8
//} states_t;
//
//const char * const states_names[ST_MAX] = {
//    [ST_INIT]           = "st_init",
//    [ST_TRANSPORT]      = "st_transport",
//    [ST_MONITOR]        = "st_monitor_src",
//    [ST_SRC_ADAPTOR]    = "st_src_adaptor",
//    [ST_SRC_BATTERY]    = "st_src_bat",
//    [ST_POWER_OFF]      = "st_poweroff",
//    [ST_TEST_END]       = "st_testend",
//    [ST_SHUTDOWN]       = "st_shutdown",
//};
//
//typedef enum {
//    ERR_NONE        = 0,
//    ERR_LOAD        = 1,
//    ERR_BKP_V       = 2,
//    ERR_BKP_LOAD    = 3,
//    ERR_ADC         = 4,
//    ERR_MP          = 5,
//    ERR_MAX         = 6,
//} states_errors_t;
//
//typedef void (*states_func_t) (void);
//
//static volatile states_t state_num;
//static volatile states_t state_num_old;
//static voltages_t voltages;
//static volatile states_errors_t states_error;
//static mp_values_t mp_voltages;
//static int lock_pins = 0;
//
///****************************************************************************************
//* Global States
//****************************************************************************************/
//static void states_task(void* parameters);
//static void st_init(void);
//static void st_transport(void);
//static void st_monitor_source(void);
//static void st_source_adaptor(void);
//static void st_source_battery(void);
//static void st_poweroff(void);
//static void st_test_end(void);
//static void st_shutdown(void);
//
//static const states_func_t states[ST_MAX] = {
//    [ST_INIT]           = st_init,
//    [ST_TRANSPORT]      = st_transport,
//    [ST_MONITOR]        = st_monitor_source,
//    [ST_SRC_ADAPTOR]    = st_source_adaptor,
//    [ST_SRC_BATTERY]    = st_source_battery,
//    [ST_POWER_OFF]      = st_poweroff,
//    [ST_TEST_END]       = st_test_end,
//    [ST_SHUTDOWN]       = st_shutdown,
//};
//
//int states_init()
//{
//    state_num_old = state_num = ST_INIT;
//    states_error = ERR_NONE;
//    lock_pins = 0;
//
//
//    xTaskCreate(states_task, "states", configMINIMAL_STACK_SIZE * 2,
//            NULL, configMAX_PRIORITIES - 1, NULL);
//    return 0;
//}
//
//const char* states_get_cur_name()
//{
//    if (state_num < ST_MAX) {
//        return states_names[state_num];
//    }
//    return 0;
//}
//
//inline static int st_pin_set(pins_num_t pin)
//{
//    if (!lock_pins) {
//        pin_set(pin);
//        return -1;
//    }
//    return 0;
//}
//
//
//inline static int st_pin_reset(pins_num_t pin)
//{
//    if (!lock_pins) {
//        pin_reset(pin);
//        return -1;
//    }
//    return 0;
//}
//
//static inline int st_get_voltages(void)
//{
//    int adc_retries = ADC_RETRIES;
//    while(adc_retries--) {
//        if (adc_get_voltages(&voltages)==0) {
//            return 0;
//        } else {
//            vTaskDelay(20);
//        }
//    }
//    return -1;
//}
//
//static inline int st_get_mp_vbat(void)
//{
//    int retries = ADC_RETRIES;
//    while(retries--) {
//        if (mp_get_values(&mp_voltages)==0) {
//            return 0;
//        } else {
//            if ((mp_voltages.status & V_BAT_ERR) == 0) {
//                return 0;
//            }
//            vTaskDelay(20);
//        }
//    }
//    return -1;
//}
//
//static void st_charge_enable(uint8_t ena)
//{
//    if (ena) {
//        st_pin_reset(PIN_CHARGE_EN);
//    } else {
//        st_pin_set(PIN_CHARGE_EN);
//    }
//}
//
//inline static void st_set_host_state(host_state_t host_state)
//{
//    if (host_state & 0x1) {
//        st_pin_set(PIN_STATUS_0);
//    } else {
//        st_pin_reset(PIN_STATUS_0);
//    }
//    if (host_state & 0x2) {
//        st_pin_set(PIN_STATUS_1);
//    } else {
//        st_pin_reset(PIN_STATUS_1);
//    }
//}
//
//static void st_set_state(states_t new_state_num)
//{
//    state_num = new_state_num;
//}
//
//static void st_set_error(states_errors_t err)
//{
//    states_error = err;
//}
//
//static int st_ups_hook(void)
//{
//    uint8_t cmd = 0;
//    static uint8_t shutdown = 0;
//    static uint8_t restore = 0;
//    static uint8_t test = 0;
//    static TickType_t shutdown_time = 0;
//    static TickType_t restore_time = 0;
//    static TickType_t test_time = 0;
//    static int transport_pin = 0;
//    static int retries;
//
//
//    cmd = ups_get_host_cmd();
//
//    transport_pin = pin_get(PIN_TRANSPORT);
//    if (transport_pin) {
//        retries = 5;
//        while (--retries && transport_pin) {
//            transport_pin = pin_get(PIN_TRANSPORT);
//            vTaskDelay(1000);
//        }
//        if (!transport_pin) {
//            return 0;
//        }
//
//        ups_set_status(UPS_BATT_WORK | UPS_BATT_LOW);
//
//        retries = 30;
//        while (--retries ) {
//            TRACE_INFO("Transport pin HOOK");
//            cmd = ups_get_host_cmd();
//            if (cmd & CMD_SHUTDOWN) {
//                ups_reset_host_cmd(CMD_SHUTDOWN);
//                TRACE_INFO("Shutdown on transport hook");
//            }
//            if (cmd & CMD_RESTORE) {
//                ups_reset_host_cmd(CMD_RESTORE);
//                TRACE_INFO("Restore on transport hook");
//            }
//            if (cmd & CMD_SHUTDOWN_CANSEL) {
//                ups_reset_host_cmd(CMD_RESTORE | CMD_SHUTDOWN
//                | CMD_SHUTDOWN_CANSEL);
//                TRACE_INFO("Cansel shutdown on transport hook");
//            }
//            vTaskDelay(1000);
//        }
//        while (retries <= 0) {
//            TRACE_INFO("Power down to transport");
//            st_pin_reset(PIN_BACKUP_CONTROL);
//            st_pin_reset(PIN_DC_IN_CONTROL);
//            st_pin_reset(PIN_BAT_TEST);
//            mp_disable_batfet();
//            st_charge_enable(0);
//            mp_disable_dcdc();
//            vTaskDelay(1000);
//        }
//    }
//
//    if (cmd & CMD_SHUTDOWN) {
//        shutdown_time = xTaskGetTickCount() +
//            ((ups_get_shutdown_delay() < 5) ? 5 : ups_get_shutdown_delay()) * configTICK_RATE_HZ;
//        ups_reset_host_cmd(CMD_SHUTDOWN);
//        shutdown = 1;
//        TRACE_INFO("Shutdown after %ld sec delay", ups_get_shutdown_delay());
//    }
//    if (cmd & CMD_RESTORE) {
//        restore_time = xTaskGetTickCount() + ups_get_restore_delay() * configTICK_RATE_HZ;
//        ups_reset_host_cmd(CMD_RESTORE);
//        restore = 1;
//        TRACE_INFO("Restore after %ld sec delay", ups_get_restore_delay());
//
//    }
//    if (cmd & CMD_SHUTDOWN_CANSEL) {
//        ups_reset_host_cmd(CMD_RESTORE | CMD_SHUTDOWN
//                | CMD_SHUTDOWN_CANSEL);
//        shutdown = 0;
//        restore = 0;
//        st_set_state(ST_SRC_BATTERY);
//        st_pin_reset(PIN_BAT_TEST);
//        lock_pins = 0;
//        TRACE_INFO("Shutdown cansel");
//        return 1;
//    }
//    if (cmd & CMD_TEST) {
//        ups_reset_host_cmd(CMD_TEST);
//        if (state_num == ST_SRC_ADAPTOR) {
//            TRACE_INFO("Battery test on");
//            ups_set_status(UPS_TEST_ON);
//            if (ups_get_test_time_sec() >= 0) {
//                test_time = xTaskGetTickCount() +
//                    (ups_get_test_time_sec()>10? 10 : ups_get_test_time_sec()) * configTICK_RATE_HZ;
//            } else {
//                test_time = xTaskGetTickCount() + 10 * configTICK_RATE_HZ ;
//            }
//            test = 1;
//            st_pin_set(PIN_BAT_TEST);
//            st_charge_enable(0);
//        }
//    }
//    if (cmd & CMD_TEST_CANSEL) {
//            ups_reset_host_cmd(CMD_TEST | CMD_TEST_CANSEL);
//            st_pin_reset(PIN_BAT_TEST);
//            test = 0;
//            ups_reset_status(UPS_TEST_ON);
//            st_charge_enable(1);
//            TRACE_INFO("Battery test off, test cansel");
//    }
//    if (test) {
//        if (state_num == ST_SRC_ADAPTOR) {
//            if (xTaskGetTickCount() > test_time) {
//                st_pin_reset(PIN_BAT_TEST);
//                test = 0;
//                ups_reset_status(UPS_TEST_ON);
//                st_charge_enable(1);
//                TRACE_INFO("Battery test off, timeout");
//            }
//        } else {
//            TRACE_INFO("Battery test off, state != src_adaptor");
//            st_pin_reset(PIN_BAT_TEST);
//            test = 0;
//            ups_reset_status(UPS_TEST_ON);
//            st_charge_enable(1);
//        }
//    }
//    if (shutdown ) {
//        if (xTaskGetTickCount() > shutdown_time) {
//            shutdown = 0;
//            st_pin_reset(PIN_BACKUP_CONTROL);
//            st_pin_reset(PIN_DC_IN_CONTROL);
//            st_pin_reset(PIN_BAT_TEST);
//            lock_pins = 1;
//            st_set_state(ST_SHUTDOWN);
//            TRACE_INFO("Shutdown host");
//            return 1;
//        }
//    }
//    if (restore) {
//        if (xTaskGetTickCount() > restore_time) {
//            restore = 0;
//            st_set_state(ST_MONITOR);
//            st_pin_reset(PIN_BAT_TEST);
//            lock_pins = 0;
//            TRACE_INFO("Restore host");
//            return 1;
//        }
//    }
//    return 0;
//}
//
//static void states_task(void* parameters)
//{
//    parameters = parameters;
//
//    while(1) {
//        states[state_num]();
//        vTaskDelay(100);
//
//        if (state_num != state_num_old) {
//            TRACE_INFO("Set state from %s to %s",
//                    states_names[state_num_old], states_names[state_num]);
//            state_num_old = state_num;
//        }
//    }
//}
//
//static void st_shutdown(void)
//{
//    uint32_t comparez;
//    compara_down_level();
//    int retries = 10;
//    uint8_t should_restore = 0;
//
//    while(1) {
//        vTaskDelay(MONITOR_SLEEP);
//        comparez = compara_get_untriggered_status();
//
//        st_get_voltages();
//
//        if (st_ups_hook()) {
//            should_restore = 1;
//        }
//        if (should_restore) {
//
//            if (voltages.input_voltage > DC_IN_MIN) {
//                return;
//            } else {
//                st_pin_reset(PIN_BACKUP_CONTROL);
//                st_pin_reset(PIN_DC_IN_CONTROL);
//                st_pin_reset(PIN_BAT_TEST);
//                TRACE_INFO("Lock restore power. Low input voltage.");
//                vTaskDelay(2000);
//            }
//        }
//
//
//        if(!comparez ) {
//            if (--retries == 0) {
//                TRACE_INFO("Battery low!"
//                        " On shutdown sate.");
//                st_set_state(ST_MONITOR);
//            }
//        } else {
//            retries = 10;
//        }
//        if (pin_get(PIN_TRANSPORT) > 0) {
//            mp_disable_batfet();
//            st_set_state(ST_POWER_OFF);
//            return;
//        }
//    }
//}
//
//static void st_test_end(void)
//{
//#ifdef TEST_ENA
//    test_print_test_result();
//    while(1) {
//        vTaskDelay(100);
//    }
//#else
//    while(1) {
//        TRACE_WARNING("Should never set this state [test end]");
//        vTaskDelay(1000);
//    }
//#endif
//}
//
//static void st_poweroff(void)
//{
//    st_pin_reset(PIN_BACKUP_CONTROL);
//    st_pin_reset(PIN_DC_IN_CONTROL);
//    st_pin_reset(PIN_BAT_TEST);
//
//    vTaskDelay(3000);
//
//    while (1) {
//        mp_disable_batfet();
//        st_charge_enable(0);
//        mp_disable_dcdc();
//        vTaskDelay(1000);
//    }
//}
//
//static void st_source_battery(void)
//{
//    int monitor_timer;
//    uint32_t comparez;
//
//    st_pin_reset(PIN_DC_IN_CONTROL);
//    st_charge_enable(0);
//    mp_disable_dcdc();
//
//    while(1) {
//
//        monitor_timer = MONITOR_TIMEOUT;
//
//        while (--monitor_timer) {
//            st_set_host_state(HOST_BAT);
//            comparez = compara_get_untriggered_status();
//
//            if (st_get_voltages()) {
//                TRACE_ERROR("ADC Failed");
//                ups_set_status(UPS_FAILED);
//                st_set_host_state(HOST_ERROR);
//            }
//            if (st_ups_hook()) {
//                TRACE_INFO("Hook change status");
//                return;
//            }
//            if ( !(voltages.input_voltage < DC_IN_MAX
//                    && voltages.input_voltage > DC_IN_MIN)) {
//                TRACE_DEBUG("Input volt:  % 3d.%02d ovr volt",
//                        GET_INT_AND_FRAG(voltages.input_voltage ,100));
//                vTaskDelay(MONITOR_SLEEP);
//                monitor_timer = MONITOR_TIMEOUT;
//            } else {
//                TRACE_DEBUG("Input volt:  % 3d.%02d OK",
//                        GET_INT_AND_FRAG(voltages.input_voltage ,100));
//                vTaskDelay(MONITOR_SLEEP);
//            }
//            if (voltages.input_voltage > voltages.backup_voltage ) {
//                TRACE_DEBUG("Input volt:  % 3d.%02d > Backup V",
//                        GET_INT_AND_FRAG(voltages.input_voltage ,100));
//                ups_set_status(UPS_BATT_WORK);
//                vTaskDelay(MONITOR_SLEEP);
//            } else {
//                if (voltages.batt_voltage > VBAT_MIN
//                                            && compara_is_lvl_standard()
//                                            && comparez ) {
//                    ups_set_status(UPS_BATT_WORK);
//                    /* Check for poweroff command */
//
//                    /* If not shutdown continuous loop */
//                    monitor_timer = MONITOR_TIMEOUT;
//                    continue;
//                } else {
//                    /* Battery low */
//                    ups_set_status(UPS_BATT_WORK | UPS_BATT_LOW);
//                    /* Check for poweroff command if not after 10 sec
//                     * put to shutdown  */
//                    if (!comparez && compara_is_lvl_standard()) {
//                        compara_down_level();
//                        TRACE_INFO("Battery low! % 3d.%02d V. Stage 1",
//                                GET_INT_AND_FRAG(voltages.batt_voltage,100));
//                    } else if(!comparez && !compara_is_lvl_standard()) {
//                        TRACE_INFO("Battery low! % 3d.%02d V. Go Power OFF",
//                                GET_INT_AND_FRAG(voltages.batt_voltage,100));
//                        st_set_state(ST_POWER_OFF);
//                        return;
//                    }
//                }
//                vTaskDelay(MONITOR_SLEEP);
//                monitor_timer = MONITOR_TIMEOUT;
//                continue;
//            }
//        }
//        TRACE_INFO("Go to source adaptor");
//        vTaskDelay(1000);
//        st_pin_set(PIN_DC_IN_CONTROL);
//        vTaskDelay(1000);
//        st_set_state(ST_SRC_ADAPTOR);
//        compara_up_level();
//        return;
//    }
//}
//
//static void st_source_adaptor(void)
//{
//
//    mp_enable_dcdc();
//    mp_enable_batfet();
//    vTaskDelay(1000);
//    st_pin_set(PIN_BACKUP_CONTROL);
//    st_pin_set(PIN_DC_IN_CONTROL);
//    st_charge_enable(1);
//    vTaskDelay(100);
//    st_charge_enable(0);
//    vTaskDelay(100);
//    st_charge_enable(1);
//
//    vTaskDelay(2000);
//
//    while (1) {
//        ups_reset_status(UPS_BATT_WORK | UPS_BATT_LOW);
//        if (st_ups_hook()) {
//            return;
//        }
//
//        if (st_get_voltages()) {
//            /* ADC ERROR */
//            TRACE_ERROR("ADC Failed");
//            ups_set_status(UPS_FAILED);
//            st_set_host_state(HOST_ERROR);
//            return;
//        }
//        if ( voltages.input_voltage > voltages.backup_voltage  ){
//            if (!(voltages.backup_voltage <= DC_BACKUP_MAX
//                 && voltages.backup_voltage >= DC_BACKUP_MIN)) {
//                TRACE_ERROR("Backp V : % 3d.%02d V",
//                        GET_INT_AND_FRAG(voltages.backup_voltage ,100));
//                TRACE_ERROR("Backup V over limits");
//                ups_set_status(UPS_FAILED);
//                st_set_host_state(HOST_ERROR);
//                return;
//            }
//            if (!(voltages.input_voltage <= DC_IN_MAX
//                  && voltages.input_voltage >= DC_IN_MIN)) {
//                TRACE_INFO("Input V : % 3d.%02d V",
//                        GET_INT_AND_FRAG(voltages.input_voltage ,100));
//                st_pin_reset(PIN_DC_IN_CONTROL);
//                ups_set_status(UPS_BATT_WORK);
//                st_set_state(ST_SRC_BATTERY);
//                return;
//            }
//            if (voltages.load_voltage < voltages.backup_voltage) {
//                TRACE_ERROR("Load V: % 3d.%02d V < Backup V: % 3d.%02d",
//                        GET_INT_AND_FRAG(voltages.load_voltage ,100),
//                        GET_INT_AND_FRAG(voltages.backup_voltage ,100));
//                ups_set_status(UPS_FAILED);
//                st_set_host_state(HOST_ERROR);
//                return;
//            }
//            if (st_get_mp_vbat() == 0) {
//                if (mp_voltages.curr_chrg > 0.05) {
//                    st_set_host_state(HOST_IN_BAT_CHARG);
//                } else {
//                    st_set_host_state(HOST_NORMAL);
//                }
//            }
//
//        } else {
//            st_pin_reset(PIN_DC_IN_CONTROL);
//            ups_set_status(UPS_BATT_WORK);
//            st_set_state(ST_SRC_BATTERY);
//            return;
//        }
//        vTaskDelay(MONITOR_SLEEP);
//    }
//}
//
//static void st_monitor_source(void)
//{
//    int monitor_timer = MONITOR_TIMEOUT;
//    uint32_t comparez;
//    int retries = 100;
//    int transport_pin;
//
//    compara_up_level();
//
//    st_charge_enable(0);
//    st_pin_reset(PIN_DC_IN_CONTROL);
//    vTaskDelay(MONITOR_SLEEP);
//
//    while(1) {
//        if (st_ups_hook()) {
//            return;
//        }
//        if (st_get_voltages()==0) {
//            if (!(voltages.input_voltage < DC_IN_MAX
//                    && voltages.input_voltage > DC_IN_MIN)) {
//
//                st_pin_reset(PIN_DC_IN_CONTROL);
//                vTaskDelay(MONITOR_SLEEP);
//                monitor_timer = MONITOR_TIMEOUT;
//
//                comparez = compara_get_untriggered_status();
//                if(!comparez ) {
//                    if (--retries == 0) {
//                    TRACE_INFO("Battery low!"
//                            " On monitor sate.");
//                    }
//                } else {
//                    retries = 10;
//                }
//                transport_pin = pin_get(PIN_TRANSPORT);
//                if (transport_pin > 0) {
//                    st_set_state(ST_TRANSPORT);
//                    return;
//                }
//                continue;
//            } else {
//                if (--monitor_timer <= 0) {
//                    st_pin_set(PIN_DC_IN_CONTROL);
//                    vTaskDelay(DC_LOAD_TIMEOUT);
//
//                    monitor_timer = MONITOR_TIMEOUT;
//
//                    if (st_get_voltages()==0) {
//                        while (1){
//                            monitor_timer--;
//                            if (voltages.input_voltage <= DC_IN_MAX
//                                    && voltages.input_voltage >= DC_IN_MIN) {
//                                st_set_state(ST_SRC_ADAPTOR);
//                                return;
//                            } else {
//                                if (!monitor_timer) {
//                                    ups_set_status(UPS_FAILED);
//                                    TRACE_ERROR("Input V over limits");
//                                    st_set_host_state(HOST_ERROR);
//                                    return;
//                                }
//                            }
//                        }
//                    } else {
//                        /* ADC ERROR!! */
//                        ups_set_status(UPS_FAILED);
//                        st_set_error(ERR_ADC);
//                        TRACE_ERROR("ADC Fail");
//                        st_set_host_state(HOST_ERROR);
//                        return;
//                    }
//                }
//            }
//        } else {
//            /* ADC ERROR */
//            ups_set_status(UPS_FAILED);
//            st_set_error(ERR_ADC);
//            TRACE_ERROR("ADC Fail");
//            st_set_host_state(HOST_ERROR);
//            return;
//        }
//    }
//
//}
//
//static void st_transport(void)
//{
//    int transport_pin;
//    int retries = 5;
//
//    st_charge_enable(0);
//
//    while (1) {
//        vTaskDelay(100);
//        transport_pin = pin_get(PIN_TRANSPORT);
//        if (transport_pin < 0) {
//            /* TODO: error */
//        }
//        --retries;
//        if (transport_pin && retries == 0) {
//            st_pin_reset(PIN_BACKUP_CONTROL);
//            st_pin_reset(PIN_DC_IN_CONTROL);
//            st_pin_reset(PIN_BAT_TEST);
//            mp_disable_batfet();
//            st_charge_enable(0);
//            mp_disable_dcdc();
//            retries = 5;
//            continue;
//        } else  if (!retries){
//            mp_enable_batfet();
//            st_set_state(ST_MONITOR);
//            break;
//        }
//    }
//}
//
//static void st_init(void)
//{
//    st_set_error(ERR_NONE);
//    st_set_state(ST_TRANSPORT);
//}
//
//
///*********************************** end of states.c **************************************/
