/*
 *
 * Interrupts routines
 *
 */
	.syntax	unified
	.cpu cortex-m3

    /* Uart base address */
	.equ MDR_UART2_BASE,    0x40038000

	.equ MDR_UART_ICR_OFFS, 0x0044
	.equ MDR_UART_RIS_OFFS, 0x003c
	.equ MDR_UART_TX_IT,    0x0020
	.equ MDR_UART_RX_IT,    0x0010

	/* Port C base address */
	.equ MDR_PORTC_BASE,    0x400b8000

	.extern UartCBIn, UartCBOut
	.extern UART_SendData
	.extern cb_GetByte

	.extern fDirectOut

    .align 2
    .data
	    uart_byte: .byte  0x0

	.text
	.thumb
	.thumb_func
	.align	2
	.globl	uart2_isr_asm
	.type	uart2_isr_asm, %function

	uart2_isr_asm:
	push    {lr}

	ldr     r0, =MDR_UART2_BASE
	ldr     r1, [r0, #MDR_UART_RIS_OFFS]
	ands    r1, #MDR_UART_TX_IT
	beq     next
	mov     r1, #0
	orrs    r1, #MDR_UART_TX_IT
	orrs    r1, #MDR_UART_RX_IT
	str     r1, [r0, #MDR_UART_ICR_OFFS]

	ldr     r0, =UartCBOut
	ldr     r1, =uart_byte
	bl      cb_GetByte
	cbz     r0, next
	ldr     r0, =MDR_UART2_BASE
	ldr     r1, =uart_byte
	ldr     r1, [r1]
	bl      UART_SendData
	pop     {pc}

	next:
	mov     r0, #1
	ldr     r1, =fDirectOut
	str     r0, [r1]

    pop     {pc}

    .end
