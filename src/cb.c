#include "cb.h"

#define IncInd(ind, buf_size)   ((ind) = ((ind) < ((buf_size) - 1)) ? ((ind) + 1) : 0)

void cb_Init(TCB* pcb, char* pbuf, unsigned int buf_size)
{
	pcb->head = 0;
	pcb->tail = 0;
	pcb->cnt = 0;
	pcb->pbuf = pbuf;
	pcb->buf_size = buf_size;
}

void cb_Reset(TCB* pcb)
{
	pcb->head = 0;
	pcb->tail = 0;
	pcb->cnt = 0;
}

bool cb_AddByte(TCB* pcb, char b)
{
	if(pcb->cnt == pcb->buf_size) return false;

	pcb->pbuf[pcb->head] = b;
	IncInd(pcb->head, pcb->buf_size);
	pcb->cnt++;
	return true;
}

bool cb_GetByte(TCB* pcb, char* pb)
{
	if(!pcb->cnt) return false;

	*pb = pcb->pbuf[pcb->tail];
	IncInd(pcb->tail, pcb->buf_size);
	pcb->cnt--;
	return true;
}

unsigned int cb_GetByteCnt(TCB* pcb)
{
	return pcb->cnt;
}

bool cb_IsEmpty(TCB* pcb)
{
	return true;
}

