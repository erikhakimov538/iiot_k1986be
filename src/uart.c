/************************************************************************************//**
* \file         inc/uart.c
* \brief        Power_sys application source file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include <string.h>
#include <stdio.h>

#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_uart.h"

#include "cb.h"
#include "uart.h"
#include "itmr.h"

/****************************************************************************************
* Global variables
****************************************************************************************/

#define BUF_SIZE 512

static char InBuf[BUF_SIZE], OutBuf[BUF_SIZE];
static TCB UartCBIn, UartCBOut;
static bool fDirectOut;


void uart_init(unsigned int speed, unsigned short bit_cnt,
                    unsigned short stop_bits, unsigned short parity)
{
    UART_InitTypeDef UART_InitStructure;

    RST_CLK_PCLKcmd( RST_CLK_PCLK_UART2, ENABLE);
    UART_BRGInit(MDR_UART2, UART_HCLKdiv16);

	NVIC_SetPriority(UART2_IRQn, 1);

	UART_InitStructure.UART_BaudRate    =  speed;
	UART_InitStructure.UART_WordLength  = bit_cnt;
	UART_InitStructure.UART_StopBits    = stop_bits;
	UART_InitStructure.UART_Parity      = parity;

	UART_InitStructure.UART_FIFOMode = UART_FIFO_OFF;
	UART_InitStructure.UART_HardwareFlowControl = UART_HardwareFlowControl_RXE
                                                    | UART_HardwareFlowControl_TXE;

	UART_Init(MDR_UART2, &UART_InitStructure) ;

    UART_ITConfig(MDR_UART2, UART_IT_TX | UART_IT_RX, ENABLE);
	UART_Cmd(MDR_UART2, ENABLE);

	cb_Init(&UartCBIn, InBuf, BUF_SIZE);
	cb_Init(&UartCBOut, OutBuf, BUF_SIZE);
	fDirectOut = true;

    NVIC_EnableIRQ(UART2_IRQn);
}

//
bool uart_send_byte(char byte)
{
	bool res;

    NVIC_DisableIRQ(UART2_IRQn);

    if (fDirectOut) {
        fDirectOut = false;
		UART_SendData(MDR_UART2, byte);
        res = true;
    } else {
        res = cb_AddByte(&UartCBOut, byte);
    }
	NVIC_EnableIRQ(UART2_IRQn);
	return res;
}

bool uart_send_str(char* str)
{
	bool res = false;

	while(*str != 0) {
		if((res = uart_send_byte(*str++)) == false) {
            break;
        }
    }
	return res;
}

bool uart_send_buf(char* buf, unsigned int size)
{
	unsigned int ind;

	for(ind = 0; ind < size; ind++) {
		uart_send_byte(buf[ind]);
    }
	return true;
}

bool uart_get_byte(char* pb)
{
	bool res;
	NVIC_DisableIRQ(UART2_IRQn);
	res = cb_GetByte(&UartCBIn, pb);
	NVIC_EnableIRQ(UART2_IRQn);
	return res;
}

void uart_irq(void)
{
	short hw;
	char b;
	if(UART_GetITStatus(MDR_UART2, UART_IT_RX) == SET) {

		UART_ClearITPendingBit(MDR_UART2, UART_IT_RX);
		hw = UART_ReceiveData(MDR_UART2);

		cb_AddByte(&UartCBIn, (char)(hw & 0xFF));
	}

	if(UART_GetITStatus(MDR_UART2, UART_IT_TX) == SET) {
		UART_ClearITPendingBit(MDR_UART2, UART_IT_TX);

		if(cb_GetByte(&UartCBOut, &b)){
			hw = b;
			UART_SendData(MDR_UART2, hw);
		}
		else{
			fDirectOut = true;
		}
	}
}

uint32_t uart_get_received_count(void) {
    return (cb_GetByteCnt(&UartCBIn));
}
/*********************************** end of uart.c **************************************/
