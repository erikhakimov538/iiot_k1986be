/************************************************************************************//**
* \file         inc/trace.c
* \brief        Power_sys application source file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "FreeRTOS.h"
#include "portmacro.h"
#include "trace.h"

#include "MDR32F9Qx_config.h"
#include "system_MDR32F9Qx.h"
/****************************************************************************************
 * Internal variables
* ****************************************************************************************/

#if !defined(NOTRACE) && (DYN_TRACES == 1)
    unsigned int traceLevel = TRACE_LEVEL;
#endif

#if !defined(NOTRACE)
    char trace_buf[TRACE_MAX_BUF_LEN];
#endif
/****************************************************************************************
* Function prototypes
****************************************************************************************/
static void itm_send_buf(char* buf, int size);

void trace_printf(const char* format, ...)
{
    int len;
    va_list ap;

    va_start(ap, format);
    len = vsnprintf(trace_buf, sizeof(trace_buf), format, ap); 
    va_end(ap);
    itm_send_buf(trace_buf, len);

}
/************************************************************************************//**
** \brief     Initializes the ADC hardware.
** \return    none.
**
****************************************************************************************/
static void itm_send_buf(char* buf, int size)
{
    int i;
    for (i=0; i<size; ++i) {
        ITM_SendChar(buf[i]);
    }
}

/*********************************** end of trace.c **************************************/

