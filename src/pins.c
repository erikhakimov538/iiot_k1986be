/************************************************************************************//**
* \file         inc/pins.c
* \brief        Power_sys application module file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_port.h"
#include "pins.h"
#include "trace.h"

MODULE_NAME("pins.c");

typedef struct {
    MDR_PORT_TypeDef* port;
    uint32_t pin;
} pins_t;

static const pins_t supported_pins[PIN_MAX] = {
    [PIN_TRANSPORT] = { MDR_PORTA, PORT_Pin_4  },
    [PIN_DC_IN_CONTROL] = { MDR_PORTA, PORT_Pin_1  },
    [PIN_BACKUP_CONTROL] = { MDR_PORTA, PORT_Pin_2  },
    [PIN_CHARGE_EN] = { MDR_PORTA, PORT_Pin_3  },
    [PIN_BAT_TEST] = { MDR_PORTA, PORT_Pin_5  },
    [PIN_STATUS_0] = { MDR_PORTB, PORT_Pin_5  },
    [PIN_STATUS_1] = { MDR_PORTB, PORT_Pin_6  },
};

int pin_set(pins_num_t pin)
{
    if (pin >= PIN_MAX) {
        return -1;
    }
    PORT_WriteBit(supported_pins[pin].port, supported_pins[pin].pin, Bit_SET);
    return 0;
}

int pin_reset(pins_num_t pin)
{
    if (pin >= PIN_MAX) {
        return -1;
    }
    PORT_WriteBit(supported_pins[pin].port, supported_pins[pin].pin, Bit_RESET);
    return 0;
}

int pin_get(pins_num_t pin)
{
    if (pin >= PIN_MAX) {
        return -1;
    }
    return PORT_ReadInputDataBit(supported_pins[pin].port, supported_pins[pin].pin);
}

/*********************************** end of pins.c **************************************/

