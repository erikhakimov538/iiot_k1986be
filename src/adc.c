///************************************************************************************//**
//* \file         inc/adc.c
//* \brief        Power_sys application source file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//#include "FreeRTOS.h"
//#include "MDR32Fx.h"
//#include "task.h"
//#include "semphr.h"
//
//#include "MDR32F9Qx_config.h"
//#include "MDR32F9Qx_adc.h"
//#include "MDR32F9Qx_rst_clk.h"
//#include "adc.h"
//#include "types.h"
//#include "trace.h"
//
//MODULE_NAME("adc.c");
//
//typedef struct {
//    uint16_t values[FILT_ORDER];
//} values_t ;
///****************************************************************************************
//* Exported variables
//****************************************************************************************/
//extern vuint32_t ADC1_Value;
//
//
//static values_t samples[CHANNELS_MAX];
//static values_t values_copy[CHANNELS_MAX];
//static int orders[CHANNELS_MAX];
//static const float channels_koefs[CHANNELS_MAX] = {
//    0.2, 0.2, 0.2, 0.66, 0.66, 1.0
//};
//
//static voltages_t voltage_values = {
//    .input_voltage = 0,
//    .backup_voltage = 0,
//    .load_voltage = 0,
//    .batt_voltage = 0,
//    .system_voltage = 0,
//    .vref_voltage = 0,
//};
//
//static voltages_t voltage_values_local = {
//    .input_voltage = 0,
//    .backup_voltage = 0,
//    .load_voltage = 0,
//    .batt_voltage = 0,
//    .system_voltage = 0,
//    .vref_voltage = 0,
//};
//
//static SemaphoreHandle_t values_lock;
///****************************************************************************************
//* Function prototypes
//****************************************************************************************/
//static void adc_init_hardware(void);
//static void adc_task(void* parameters);
//static unsigned int median_filter(uint16_t next_value,
//                            uint16_t* values_copy,
//                            uint16_t* samples, int *order);
//
///************************************************************************************//**
//** \brief     Initializes the ADC.
//** \return    none.
//**
//****************************************************************************************/
//int adc_init(void)
//{
//    adc_init_hardware();
//    xTaskCreate(adc_task, "adc", configMINIMAL_STACK_SIZE * 2,
//            NULL, configMAX_PRIORITIES - 1, NULL);
//    values_lock = xSemaphoreCreateBinary();
//    xSemaphoreGive(values_lock);
//
//    return 0;
//}
//
///************************************************************************************//**
//** \brief     Initializes the ADC hardware.
//** \return    none.
//**
//****************************************************************************************/
//static void adc_init_hardware(void)
//{
//    TRACE_INFO("Initialize ADC");
//
//    ADC_InitTypeDef  ADC_InitStructure;
//    ADCx_InitTypeDef ADCx_InitStructure;
//
//    /* Enable the RTCHSE clock on ADC1 */
//    RST_CLK_PCLKcmd(RST_CLK_PCLK_ADC, ENABLE);
//
//    /* ADC Configuration */
//    /* Reset all ADC settings */
//    ADC_DeInit();
//
//    ADC_StructInit(&ADC_InitStructure);
//    ADC_InitStructure.ADC_SynchronousMode      = ADC_SyncMode_Independent;
//    /* Use base voltage source to reduce the power consumption,
//     * thus enable the temperature sensor */
//    ADC_InitStructure.ADC_TempSensor           = ADC_TEMP_SENSOR_Enable;
//    ADC_InitStructure.ADC_TempSensorAmplifier  = ADC_TEMP_SENSOR_AMPLIFIER_Enable;
//    ADC_InitStructure.ADC_IntVRefConversion    = ADC_VREF_CONVERSION_Enable;
//    ADC_Init(&ADC_InitStructure);
//
//    ADCx_StructInit(&ADCx_InitStructure);
//
//    /* Enable ADC cyclic mode */
//    ADCx_InitStructure.ADC_SamplingMode     = ADC_SAMPLING_MODE_SINGLE_CONV;
//    ADCx_InitStructure.ADC_Channels         = ADC_CH_ADC0_MSK | ADC_CH_ADC1_MSK | ADC_CH_ADC2_MSK | ADC_CH_ADC3_MSK | ADC_CH_ADC4_MSK | ADC_CH_INT_VREF_MSK;
//    ADCx_InitStructure.ADC_ChannelNumber    = ADC_CH_ADC0_MSK | ADC_CH_ADC1_MSK | ADC_CH_ADC2_MSK | ADC_CH_ADC3_MSK | ADC_CH_ADC4_MSK | ADC_CH_INT_VREF_MSK;
//    ADCx_InitStructure.ADC_IntVRefSource    = ADC_INT_VREF_SOURCE_EXACT;
//    ADCx_InitStructure.ADC_VRefSource       = ADC_VREF_SOURCE_INTERNAL;
//    ADCx_InitStructure.ADC_Prescaler        = ADC_CLK_div_32768;
//    ADCx_InitStructure.ADC_DelayGo          = 0x7;
//    ADCx_InitStructure.ADC_ChannelSwitching   = ADC_CH_SWITCHING_Enable;
//
//    ADC1_Init(&ADCx_InitStructure);
//    ADC1_Cmd(ENABLE);
//}
//
///************************************************************************************//**
//** \brief     Get voltages data
//** \param[in] v voltages data pointer.
//** \return    result of operation, if 0 success.
//**
//****************************************************************************************/
//int adc_get_voltages(voltages_t* v)
//{
//
//    if (xSemaphoreTake(values_lock, (TickType_t)10 )) {
//        *v = voltage_values;
//        xSemaphoreGive(values_lock);
//    } else {
//        return -1;
//    }
//    return 0;
//}
///************************************************************************************//**
//** \brief     ADC task.
//** \return    none.
//**
//****************************************************************************************/
//static void adc_task(void* parameters)
//{
//    uint16_t channel = 0;
//    float value_f = 0;
//    uint32_t value_i = 0;
//    unsigned int value = 0;
//
//    while(1) {
//        ADC1_Value = 0;
//        ADC1_Start();
//
//        while (!ADC1_GetFlagStatus(ADCx_FLAG_END_OF_CONVERSION)) {
//            vTaskDelay(1);
//        }
//        value = ADC1_GetResult();
//        channel = (value >> 16) & 0x1f /* channel mask */;
//        if (channel == 30) {
//            channel = 5;
//        }
//
//        if (channel >= CHANNELS_MAX) {
//            TRACE_FATAL( "ADC channel error:%d", channel);
//        } else {
//            value_i = median_filter(value & 0x00000fff, values_copy[channel].values, samples[channel].values, &(orders[channel]));
//            value_f = value_i;
//            if (channel == 5) {
//                value_f = (value_f / 0xd09) * 3.3;
//            } else {
//                value_f = value_f / (float)0x0fff * 3.3  /*  voltage_values.vref_voltage */  / channels_koefs[channel];
//            }
//
//            if (channel == 0) {
//                voltage_values_local.input_voltage = value_f;
//                voltage_values_local.input_code = value_i;
//            } else if (channel == 1) {
//                voltage_values_local.backup_voltage = value_f;
//                voltage_values_local.backup_code = value_i;
//            } else if (channel == 2) {
//                voltage_values_local.load_voltage = value_f;
//                voltage_values_local.load_code = value_i;
//            } else if (channel == 3) {
//                voltage_values_local.batt_voltage = value_f;
//                voltage_values_local.batt_code = value_i;
//            } else if (channel == 4) {
//                voltage_values_local.system_voltage = value_f;
//                voltage_values_local.system_code = value_i;
//            }  else if (channel == 5) {
//                voltage_values_local.vref_voltage = value_f;
//                voltage_values_local.vref_code = value_i;
//            }
//
//
//            if (channel == CHANNELS_MAX-1) {
//                if (xSemaphoreTake(values_lock, (TickType_t)20 )) {
//                    voltage_values = voltage_values_local;
//                    xSemaphoreGive(values_lock);
//                }
//                vTaskDelay(100);
//            } else {
//            }
//
//
//        }
//    }
//}
//
///************************************************************************************//**
//** \brief     Median filter.
//** \param[in] next_value next filter value.
//** \param[in] copy values array.
//** \param[in] samples values array.
//** \return    filtered value.
//**
//** TODO: Refactoring needed
//****************************************************************************************/
//static unsigned int median_filter(uint16_t next_value,
//                            uint16_t* values_copy,
//                            uint16_t* samples, int *order)
//{
//    int j,k;
//    uint32_t sum = 0;
//    float average = 0;
//    uint16_t max_value = 0xffff;
//
//    if (*order < FILT_ORDER) {
//        samples[(*order)++] = next_value;
//        return next_value;
//    }
//    samples[(*order)++%FILT_ORDER] = next_value;
//    memcpy(values_copy, samples, FILT_ORDER*2);
//
//    for (j=0; j<FILT_ORDER-1; j++) {
//        for(k=j+1; k<FILT_ORDER; k++) {
//            if (values_copy[j] < values_copy[k]) {
//                max_value = values_copy[k];
//                values_copy[k] = values_copy[j];
//                values_copy[j] = max_value;
//            }
//        }
//    }
//    return values_copy[FILT_ORDER/2];
//}
//
///*********************************** end of adc.c **************************************/
