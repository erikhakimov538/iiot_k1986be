///************************************************************************************//**
//* \file         inc/mp2731.c
//* \brief        Power_sys application module file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//#include "MDR32F9Qx_i2c.h"
//#include "MDR32F9Qx_port.h"
//#include "MDR32F9Qx_rst_clk.h"
//#include "FreeRTOS.h"
//#include "task.h"
//#include "semphr.h"
//
//#include "trace.h"
//#include "mp2731.h"
//
//
///****************************************************************************************
//* Global Definitions
//****************************************************************************************/
//#define MP_ADDR         (0x4b<<1)
//
//#define MP_REG_ADC_CTRL 0x03
//
//#define MP_REG_BATFET   0x0a
//#define MP_REG_VBAT     0x0e
//#define MP_REG_VSYS     0x0f
//#define MP_REG_VNTC     0x10
//#define MP_REG_VIN      0x11
//#define MP_REG_CURCHRG  0x12
//#define MP_REG_CURIN    0x13
//#define MP_REG_TIMERS   0x08
//#define MP_REG_CURLIM   0x00
//#define MP_REG_VINMIN   0x01
//
//#define DELAY_LOOP_CYCLES               (8UL)
//#define GET_US_LOOPS(N)                 ((uint32_t)((float)(N) * FLASH_PROG_FREQ_MHZ / DELAY_LOOP_CYCLES))
//
//#define MP_MAX_RETRIES  (GET_US_LOOPS(500000))
//
//MODULE_NAME("mp2731.c");
//
//static mp_values_t mp_values;
//
//typedef enum {
//    MP_VBAT         = 0,
//    MP_VIN          = 1,
//    MP_VSYS         = 2,
//    MP_VNTC         = 3,
//    MP_CURR_CHGR    = 4,
//    MP_CUR_IN_LOCAL = 5,
//    MP_VAL_IND_MAX  = 6,
//} mp_values_index_t;
//
//typedef struct {
//    const char* name;
//    float               v;
//    uint8_t             i2c_register;
//    float               koeff;
//    mp_values_status_t  error_code;
//    uint8_t             error;
//} mp_local_values_t;
//
///****************************************************************************************
//* Global Variables
//****************************************************************************************/
//static mp_local_values_t mp_local_values[MP_VAL_IND_MAX] = {
//    { "v_bat",      0,  MP_REG_VBAT,      (20.0 /1000.0), V_BAT_ERR            , 0 },
//    { "v_in",       0,  MP_REG_VIN,       (60.0 /1000.0), V_IN_ERR             , 0 },
//    { "v_ntc",      0,  MP_REG_VSYS,      (20.0 /1000.0), V_SYS_ERR            , 0 },
//    { "v_sys",      0,  MP_REG_VNTC,      (392.0/1000.0), V_NTC_ERR            , 0 },
//    { "cur_chgr",   0,  MP_REG_CURCHRG,   (17.5 /1000.0), V_CURR_CHRG_ERR      , 0 },
//    { "cur_loc_in", 0,  MP_REG_CURIN,     (13.3 /1000.0), V_CURR_IN_LOCAL_ERR  , 0 },
//};
//
//static SemaphoreHandle_t values_lock;
///****************************************************************************************
//* Function prototypes
//****************************************************************************************/
//static void mp_task(void* parameters);
//static int mp_initialized = 0;
///************************************************************************************//**
//** \brief     Initializes the MP2731 chip
//** \return    0 if all ok.
//**
//****************************************************************************************/
//int mp_i2c_init(void)
//{
//    I2C_InitTypeDef I2C_InitStruct;
//
//    /* Enables the HSI clock on I2C */
//    RST_CLK_PCLKcmd(RST_CLK_PCLK_I2C,ENABLE);
//
//    /* Enables I2C peripheral */
//    I2C_Cmd(ENABLE);
//
//    /* Initialize I2C_InitStruct */
//    I2C_InitStruct.I2C_ClkDiv = 16;
//    I2C_InitStruct.I2C_Speed = I2C_SPEED_UP_TO_400KHz;
//
//    /* Configure I2C parameters */
//    I2C_Init(&I2C_InitStruct);
//
//    values_lock = xSemaphoreCreateBinary();
//    xSemaphoreGive(values_lock);
//
//    xTaskCreate(mp_task, "mp2731", configMINIMAL_STACK_SIZE * 2,
//            NULL, configMAX_PRIORITIES - 1, NULL);
//
//    mp_initialized = 1;
//
//    return 0;
//}
//
//static int i2c_wait_bus_free(void)
//{
//    volatile uint32_t retries = MP_MAX_RETRIES;
//
//    while ((I2C_GetFlagStatus(I2C_FLAG_BUS_FREE) != SET)
//            && (retries > 0)) {
//        --retries;
//    }
//    if (retries == 0) {
//        TRACE_ERROR("Fail lock I2C bus");
//        return -1;
//    }
//    return 0;
//}
//
//static int i2c_wait_transfer(void)
//{
//    volatile uint32_t retries = MP_MAX_RETRIES;
//    while ((I2C_GetFlagStatus(I2C_FLAG_nTRANS) != SET)
//            && (retries>0)) {
//        --retries;
//    }
//    if (retries == 0) {
//        TRACE_ERROR("Fail transmit");
//        return -2;
//    }
//    return 0;
//}
//
//static int mp_set_reg(uint8_t reg_addr, uint8_t data)
//{
//    int ret = 0;
//
//    portENTER_CRITICAL();
//
//    /* Wait I2C bus is free */
//    if (i2c_wait_bus_free()) {
//        ret = -1;
//        goto err_out;
//    }
//
//    I2C_Send7bitAddress(MP_ADDR,I2C_Direction_Transmitter);
//    /* Wait end of transfer */
//    if (i2c_wait_transfer()) {
//        ret = -2;
//        goto err_out;
//    }
//
//    if (I2C_GetFlagStatus(I2C_FLAG_SLAVE_ACK) != SET) {
//        I2C_SendSTOP();
//        ret = -3;
//        goto err_out;
//    }
//
//    I2C_SendByte(reg_addr);
//    /* Wait end of transfer */
//    if (i2c_wait_transfer()) {
//        ret = -4;
//        goto err_out;
//    }
//
//    if (I2C_GetFlagStatus(I2C_FLAG_SLAVE_ACK) != SET) {
//        I2C_SendSTOP();
//        ret = -5;
//        goto err_out;
//    }
//
//    I2C_SendByte(data);
//    /* Wait end of transfer */
//    if (i2c_wait_transfer()) {
//        ret = -6;
//        goto err_out;
//    }
//
//    if (I2C_GetFlagStatus(I2C_FLAG_SLAVE_ACK) != SET) {
//        I2C_SendSTOP();
//        ret = -7;
//        goto err_out;
//    }
//    I2C_SendSTOP();
//    portEXIT_CRITICAL();
//    return 0;
//
//err_out:
//    portEXIT_CRITICAL();
//    return ret;
//}
//
//static int mp_get_reg(uint8_t reg_addr, uint8_t* data)
//{
//    int ret = 0;
//
//    portENTER_CRITICAL();
//    /* Wait I2C bus is free */
//    if (i2c_wait_bus_free()) {
//        ret = -100;
//        goto err;
//    }
//
//    I2C_Send7bitAddress(MP_ADDR,I2C_Direction_Transmitter);
//
//    /* Wait end of transfer */
//    if (i2c_wait_transfer()) {
//        ret = -101;
//        goto err;
//    }
//
//    if (I2C_GetFlagStatus(I2C_FLAG_SLAVE_ACK) != SET) {
//        I2C_SendSTOP();
//        ret = -102;
//        goto err;
//
//    }
//
//    I2C_SendByte(reg_addr);
//
//    /* Wait end of transfer */
//    if (i2c_wait_transfer()) {
//        ret = -103;
//        goto err;
//    }
//
//   if (I2C_GetFlagStatus(I2C_FLAG_SLAVE_ACK) != SET) {
//       I2C_SendSTOP();
//       ret = -104;
//       goto err;
//   }
//
//   /* Send adress */
//   I2C_Send7bitAddress(MP_ADDR,I2C_Direction_Receiver);
//
//    if (i2c_wait_transfer()) {
//        ret = -105;
//        goto err;
//    }
//
//    /* Recive data if ACK was send */
//    if (I2C_GetFlagStatus(I2C_FLAG_SLAVE_ACK) == SET) {
//        /* Recive byte and send ack */
//        I2C_StartReceiveData(I2C_Send_to_Slave_NACK);
//
//            /* Wait end of transfer */
//        if (i2c_wait_transfer()) {
//            ret = -106;
//            goto err;
//        }
//
//        /* Get data from I2C RXD register */
//        *data = I2C_GetReceivedData();
//    }
//
//    /* Send stop */
//    I2C_SendSTOP();
//    portEXIT_CRITICAL();
//    return ret;
//
//err:
//    portEXIT_CRITICAL();
//    return ret;
//}
//
//int mp_enable_adc_vbat(void)
//{
//    uint8_t data;
//    int ret = 0;
//    if ((ret = mp_get_reg(MP_REG_ADC_CTRL, &data))) {
//        return ret;
//    }
//    data |= 1<<6;
//    if ((ret = mp_set_reg(MP_REG_ADC_CTRL, data))) {
//        return ret;
//    }
//    return ret;
//}
//
//int mp_disable_dcdc(void)
//{
//    uint8_t data;
//    int ret = 0;
//
//    if ((ret = mp_get_reg(MP_REG_CURLIM, &data))) {
//        return ret;
//    }
//    data |= (1<<7);
//    if ((ret = mp_set_reg(MP_REG_CURLIM, data))) {
//        return ret;
//    }
//    TRACE_INFO("Disable dcdc 0x%02x", (uint32_t)data);
//    return ret;
//}
//
//int mp_enable_dcdc(void)
//{
//    uint8_t data;
//    int ret = 0;
//    if ((ret = mp_get_reg(MP_REG_CURLIM, &data))) {
//        return ret;
//    }
//    data &= ~(1<<7);
//    if ((ret = mp_set_reg(MP_REG_CURLIM, data))) {
//        return ret;
//    }
//    TRACE_INFO("Enable dcdc 0x%02x", (uint32_t)data);
//    return ret;
//}
//
//int mp_disable_batfet(void)
//{
//    uint8_t data;
//    int ret = 0;
//    if ((ret = mp_get_reg(MP_REG_BATFET, &data))) {
//        return ret;
//    }
//    data |= (1<<5);
//    if ((ret = mp_set_reg(MP_REG_BATFET, data))) {
//        return ret;
//    }
//    return ret;
//}
//
//int mp_enable_batfet(void)
//{
//    uint8_t data;
//    int ret = 0;
//    if ((ret = mp_get_reg(MP_REG_BATFET, &data))) {
//        return ret;
//    }
//    data &= ~(1<<5);
//    data |=  (1<<4);
//
//    if ((ret = mp_set_reg(MP_REG_BATFET, data))) {
//        return ret;
//    }
//    return ret;
//}
//
//inline static int mp_meassure_values(void)
//{
//    int ret = 0;
//    uint8_t data = 0;
//    int i;
//
//    for (i=0; i < MP_VAL_IND_MAX; i++) {
//        ret = mp_get_reg(mp_local_values[i].i2c_register, &data);
//        if (ret) {
//            TRACE_ERROR("Error get %s : %d", mp_local_values[i], ret);
//            mp_local_values[i].error = mp_local_values[i].error_code;
//        } else {
//            mp_local_values[i].v = data * mp_local_values[i].koeff;
//            mp_local_values[i].error = 0;
//            ret = -1;
//        }
//    }
//    // reset watchdog
//    //ret = mp_get_reg(MP_REG_TIMERS, &data);
//    //data |= (1<<3);
//    //mp_set_reg(MP_REG_TIMERS, data);
//
//    //data = 0x70;
//    //mp_set_reg(MP_REG_VINMIN, data);
//    return 0;
//}
//
//static void mp_task(void* parameters)
//{
//    int ret = 0;
//
//    parameters = parameters;
//
//    while (1) {
//
//        ret = mp_meassure_values();
//
//        if (xSemaphoreTake(values_lock, (TickType_t)10 )) {
//            mp_values.status = 0;
//
//            mp_values.v_in          = mp_local_values[MP_VIN].v;
//            mp_values.v_bat         = mp_local_values[MP_VBAT].v;
//            mp_values.v_sys         = mp_local_values[MP_VSYS].v;
//            mp_values.v_ntc         = 60 - (mp_local_values[MP_VNTC].v - 34.0) * 1.578;
//            mp_values.curr_in_local = mp_local_values[MP_CUR_IN_LOCAL].v;
//            mp_values.curr_chrg     = mp_local_values[MP_CURR_CHGR].v;
//
//            if (ret) {
//                mp_values.status |= mp_local_values[MP_VIN].error;
//                mp_values.status |= mp_local_values[MP_VBAT].error;
//                mp_values.status |= mp_local_values[MP_VSYS].error;
//                mp_values.status |= mp_local_values[MP_VNTC].error;
//                mp_values.status |= mp_local_values[MP_CUR_IN_LOCAL].error;
//                mp_values.status |= mp_local_values[MP_CURR_CHGR].error;
//            }
//
//            xSemaphoreGive(values_lock);
//        }
//        vTaskDelay(200);
//    }
//}
//
//mp_status_errors_t mp_get_values(mp_values_t* v)
//{
//    int ret = 0;
//    if (!v)
//        return EPARM;
//
//    if (!mp_initialized) {
//        return EUNINIT;
//    }
//
//    if (xSemaphoreTake(values_lock, (TickType_t)100 )) {
//        *v = mp_values;
//        if (mp_values.status) {
//            ret = EVALUE;
//        }
//        xSemaphoreGive(values_lock);
//    } else {
//        return ELOCK;
//    }
//    return ret;
//}
//
///*********************************** end of mp2731.c **************************************/
