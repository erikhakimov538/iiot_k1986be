/************************************************************************************//**
* \file         inc/main.c
* \brief        Power_sys application source file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_uart.h"
#include "MDR32F9Qx_ssp.h"
#include "FreeRTOS.h"
#include "task.h"

#include "trace.h"
#include "adc.h"
#include "compara.h"
#include "uart.h"
#include "ups_commands.h"
#include "mp2731.h"
#include "states.h"

#include "spi.h"
#include "RF24.h"

MODULE_NAME("main.c");

/****************************************************************************************
* Exported variables
****************************************************************************************/
extern void delay_asm(uint32_t nCount);

/****************************************************************************************
* Function prototypes
****************************************************************************************/
static void system_clocks_init(void );

/****************************************************************************************
* Global variables
****************************************************************************************/
//const uint64_t pipe0 = 0x787878787878;	// pipe #0
const uint64_t pipe1 = 0xE8E8F0F0E1LL;  	// pipe #1
//const uint64_t pipe2 = 0xE8E8F0F0A2LL;	// pipe #2
//const uint64_t pipe3 = 0xE8E8F0F0D1LL;	// pipe #3
//const uint64_t pipe4 = 0xE8E8F0F0C3LL;	// pipe #4
//const uint64_t pipe5 = 0xE8E8F0F0E7LL;	// pipe #5
extern uint32_t	TransferStatus;

//======================================================================================
void reciver_task(void* parameters) {
// Разрешаем добавлять полезную нагрузку в автоподтверждении
	enableAckPayload();
// Частотный канал
	setChannel(19);
// Открытие труб для приема
	openReadingPipe(1, pipe1);
//	openReadingPipe(2, pipe2);
//	openReadingPipe(3, pipe3);
//	openReadingPipe(4, pipe4);
//	openReadingPipe(5, pipe5);
//	enableDynamicPayloads();

// Запускаем прослушивание труб
	startListening();

         while(1) {
                uint8_t recive_data[32] = {0,}; // буфер указываем максимального размера
                static uint8_t remsg = 0;
                uint8_t pipe_num = 0;

                // проверяем пришло ли что-то
                if(available(&pipe_num)) {
                        remsg++;

                        writeAckPayload(pipe_num, &remsg, sizeof(remsg)); // отправляем полезную нагрузку вместе с подтверждением
                        // проверяем в какую трубу пришли данные
                        if(pipe_num == 0) {
                        	    TRACE_INFO_WP("pipe 0\n");
                        } else if(pipe_num == 1) {
                                TRACE_INFO_WP("pipe 1\n");
                                // смотрим сколько байт прилетело
                                uint8_t count = getDynamicPayloadSize();
                                // Читаем данные в массив и указываем сколько байт читать
                                read(&recive_data, count);
                                // проверяем правильность данных
                                if(recive_data[0] == 77 && recive_data[1] == 86 && recive_data[2] == 97) {
                                        TRACE_INFO_WP("data[0]=%d data[1]=%d data[2]=%d\n", recive_data[0], recive_data[1], recive_data[2]);
                                }
                        } else if(pipe_num == 2) {
                            	TRACE_INFO_WP("pipe 2\n");
                        } else {
                                // если данные придут от неуказанной трубы, то попадут сюда
                                while(availableMy()) {
                                        read(&recive_data, sizeof(recive_data));
                                        TRACE_INFO_WP("Unknown pipe\n");
                                }
                        }
                }
         }
}

//==============================================================================
void transmiter_task(void* parameters) {

    while(1) {
        uint8_t transmit_data[32] = {0,};
        transmit_data[0] = 77;
        transmit_data[1] = 86;
        transmit_data[2] = 97;

        // переменная для приёма байта пришедшего вместе с ответом
        uint8_t remsg = 0;
        // отправка данных
        if(write(&transmit_data, strlen((const char*)transmit_data))) {
        	TRACE_INFO_WP("Data sent to ESP32!!!\n");
                // проверяем пришло ли что-то вместе с ответом
        	if(isAckPayloadAvailable()) {
        		read(&remsg, sizeof(remsg));
        		TRACE_INFO_WP("Ack: %d\n", remsg);
        	}
        } else {
        		TRACE_INFO_WP("Not write!\n");
        	}
        vTaskDelay(1000);
    }
}

/****************************************************************************************
** \brief     This is the entry point for the power_sys application and is called
**            by the reset interrupt vector after the C-startup routines executed.
** \return    Program return code.
**
****************************************************************************************/
int main(void) {
    uint32_t  timeout = 0xfffff;
    uint8_t res;

    while( timeout-- ) { };

    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);

    system_clocks_init();

//--------------------	Новая инициализация	--------------------------------
//	Работает при частоте SWD = 500 кГц
//    RST_CLK_CPU_PLLconfig (RST_CLK_CPU_PLLsrcHSIdiv2, RST_CLK_CPU_PLLmul10);
//    RST_CLK_CPU_PLLcmd(ENABLE);
//
//    RST_CLK_CPU_PLLuse(ENABLE);
//    RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
//
//    RST_CLK_PCLKcmd((RST_CLK_PCLK_PORTA | RST_CLK_PCLK_PORTB | RST_CLK_PCLK_PORTC | RST_CLK_PCLK_PORTE | RST_CLK_PCLK_PORTF | RST_CLK_PCLK_PORTD), ENABLE);
//    RST_CLK_PCLKcmd(RST_CLK_PCLK_BKP,ENABLE);
//--------------------	Конец инициализации	--------------------------------

    TRACE_INFO_WP("\n\n---------- Start system ------------\n\n");
    TRACE_INFO_WP("Build: " __DATE__ " "  __TIME__);

    SPI_Init();
   	TRACE_INFO_WP("SPI init...");

//    res = read_register(SETUP_AW);
   	res = isChipConnected();
	if (res != 0) {
		TRACE_INFO("Chip connect status: %d", (uint32_t)res);
		}

	res = NRF_Init();
	if (res > 0 && res < 255) {
			TRACE_INFO("NRF init status: 0x%02x", (uint32_t)res);
		}

//	TRACE_INFO("Module init status: 0x%02x", (uint32_t)res);

	res = isChipConnected();
	if (res != 0) {
		TRACE_INFO("Chip connect status: %d", (uint32_t)res);
	}

	// Разрешаем добавлять полезную нагрузку в автоподтверждении
		enableAckPayload();
	// Частотный канал
		setChannel(19);
	// Открытие труб для передачи
		openWritingPipe(pipe1);

    xTaskCreate(transmiter_task, "transmiter", configMINIMAL_STACK_SIZE*6,  NULL, configMAX_PRIORITIES - 1, NULL);
    TRACE_INFO("Start task sheduler");
    vTaskStartScheduler();
    while(1);
}

/************************************************************************************//**
** \brief     Asserts from FreeRTOS code.
** \return    none.
**
****************************************************************************************/
void vAssertCalled(const char * const pcFileName, unsigned long ulLine) {
    TRACE_FATAL_WP("Assert of : %s : %d", pcFileName, (int)ulLine);
}

/************************************************************************************//**
** \brief     Initialize system clocks.
** \return    none.
**
****************************************************************************************/
static void system_clocks_init(void ) {
    RST_CLK_CPU_PLLconfig (RST_CLK_CPU_PLLsrcHSIdiv2,0);

    RST_CLK_HSEconfig(RST_CLK_HSE_ON);
    while (RST_CLK_HSEstatus() != SUCCESS);

    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTC, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTD, ENABLE);

    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTE, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTF, ENABLE);
    RST_CLK_PCLKcmd(RST_CLK_PCLK_BKP,ENABLE);

    RST_CLK_CPU_PLLconfig(RST_CLK_CPU_PLLsrcHSEdiv2, RST_CLK_CPU_PLLmul10);
    RST_CLK_CPU_PLLcmd(ENABLE);

    if (RST_CLK_CPU_PLLstatus() != SUCCESS) {
        TRACE_FATAL("Fail start PLL");
    }
    /* CPU_C3_SEL = CPU_C2_SEL */
    RST_CLK_CPUclkPrescaler(RST_CLK_CPUclkDIV1);
    /* CPU_C2_SEL = PLL */
    RST_CLK_CPU_PLLuse(ENABLE);
    /* HCLK_SEL = CPU_C3_SEL */
    RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);
}

void delay_us(uint32_t nCount)  {
    delay_asm(nCount);
}

//#pragma clang diagnostic pop
/*********************************** end of main.c **************************************/
