///************************************************************************************//**
//* \file         inc/pins.c
//* \brief        Power_sys application module file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//#include "module_test.h"
//#include "trace.h"
//#include "FreeRTOS.h"
//#include "task.h"
//#include "string.h"
//
//
//
//
//MODULE_NAME("TEST");
//
///****************************************************************************************
//* Local vars
//****************************************************************************************/
//static const char * current_test_name[] = {
//    "None"
//};
//
//static int current_test_num = 0;
//static int current_test_status = TEST_FAIL;
//static volatile int test_stage = 0;
//static volatile int test_stage_current = 0;
//static uint8_t test_is_wait_stage = 0;
//
///****************************************************************************************
//* Function definitions
//****************************************************************************************/
//static void tests_task(void* parameters);
//
///****************************************************************************************
//* Function references
//****************************************************************************************/
//int tests_init(void)
//{
//    xTaskCreate(tests_task, "tests", configMINIMAL_STACK_SIZE * 2,
//            NULL, configMAX_PRIORITIES - 1, NULL);
//    return 0;
//}
//
//void test_wait_stage(void)
//{
//    test_is_wait_stage = 1;
//}
//
//void test_print_test_result(void)
//{
//    TRACE_TEST_SLNT("[ Test %16s end %5s ]", current_test_name[current_test_num],
//            current_test_status ? "OK" : "ERROR" );
//}
//
//static void tests_task(void* parameters)
//{
//    parameters = parameters;
//
//    while(1) {
//    }
//}
//
///*********************************** end of pins.c **************************************/
