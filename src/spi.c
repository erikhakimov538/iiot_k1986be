#include <string.h>
#include <stdio.h>
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_ssp.h"
#include "cb.h"
#include "spi.h"
#include "itmr.h"

uint16_t TxIdx = 0, RxIdx = 0;
uint32_t	TransferStatus;

// Функция настройки SPI
void SPI_Init(void) {

    PORT_InitTypeDef PortInit;
    SSP_InitTypeDef  sSSP;

    RST_CLK_PCLKcmd(RST_CLK_PCLK_SSP1, ENABLE);

    /* Configure Chip Enable pin */
    PortInit.PORT_FUNC = PORT_FUNC_MAIN;			// CE
    PortInit.PORT_MODE = PORT_MODE_DIGITAL;
    PortInit.PORT_PD = PORT_PD_DRIVER;
    PortInit.PORT_SPEED = PORT_SPEED_MAXFAST;

    PortInit.PORT_Pin = PORT_Pin_7;
    PortInit.PORT_OE = PORT_OE_OUT;
    PORT_Init(MDR_PORTE, &PortInit);

    PORT_WriteBit(MDR_PORTE, PORT_Pin_7, Bit_SET);
    //	Конфигурация пинов SSP1(PORTF): FSS(0), CLK(1), RXD(2), TXD(3)
    //					   SSP2(PORTD): FSS(2), CLK(3), RXD(5), TXD(6)

    PortInit.PORT_FUNC = PORT_FUNC_ALTER;
    PortInit.PORT_MODE = PORT_MODE_DIGITAL;
    PortInit.PORT_SPEED = PORT_SPEED_FAST;
    PortInit.PORT_Pin = PORT_Pin_0 | PORT_Pin_1;
    PortInit.PORT_OE = PORT_OE_OUT;
    PORT_Init(MDR_PORTF, &PortInit);

    PortInit.PORT_Pin = PORT_Pin_3;
    PortInit.PORT_OE = PORT_OE_IN;
    PORT_Init(MDR_PORTF, &PortInit);

    PortInit.PORT_FUNC = PORT_FUNC_PORT;
    PortInit.PORT_MODE = PORT_MODE_DIGITAL;
    PortInit.PORT_SPEED = PORT_SPEED_FAST;

    PortInit.PORT_Pin = PORT_Pin_2;
    PortInit.PORT_OE = PORT_OE_OUT;
    PORT_Init(MDR_PORTF, &PortInit);

    SSP_DeInit(MDR_SSP1);

    SSP_BRGInit(MDR_SSP1_BASE, SSP_HCLKdiv16);
    SSP_StructInit(&sSSP);

    // Настройка модуля SSP в SPI_Motorola
    sSSP.SSP_CPSDVSR = 3;
    sSSP.SSP_SCR = 0x10;
    sSSP.SSP_Mode = SSP_ModeMaster;
    sSSP.SSP_WordLength = SSP_WordLength8b;
    sSSP.SSP_SPO = SSP_SPO_Low;
    sSSP.SSP_SPH = SSP_SPH_1Edge;
    sSSP.SSP_FRF = SSP_FRF_SPI_Motorola;
    sSSP.SSP_HardwareFlowControl = SSP_HardwareFlowControl_SSE;
    SSP_Init(MDR_SSP1_BASE, &sSSP);

    SSP_Cmd(MDR_SSP1_BASE, ENABLE);
}

// Функция отправки данных по SPI
void SPI1_Transmit(uint16_t *DataBuf, uint16_t DataSize)	{
	TxIdx = 0;
	volatile uint16_t tmp;
	while (TxIdx < DataSize) {
		/* Ожидание пустого буфера SPI Tx */
		while (SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_TFE) == RESET) {
		}
		/* Отправка данных */
		SSP_SendData(MDR_SSP1_BASE, DataBuf[TxIdx]);
		TxIdx++;
		while (SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_BSY) == SET) {
				}
		/* Ожидание приема данных */
		while (SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_RNE) == RESET) {
		}
		tmp = SSP_ReceiveData(MDR_SSP1_BASE);
	}
}

// Функция отправки данных по SPI с обратным чтением
void SPI1_TransmitReceive(uint16_t *DataBuf, uint16_t *ReceiveBuf, uint16_t DataSize)	{
	TxIdx = 0;
	RxIdx = 0;
	while (TxIdx < DataSize) {
		/* Ожидание пустого буфера SPI Tx */
		while (SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_TFE) == RESET) {
		}
		/* Отправка данных */
		SSP_SendData(MDR_SSP1_BASE, DataBuf[TxIdx]);
		TxIdx++;
		while (SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_BSY) == SET) {
				}
		/* Ожидание приема данных */
		while (SSP_GetFlagStatus(MDR_SSP1_BASE, SSP_FLAG_RNE) == RESET) {
		}
		/* Чтение полученных данных */
		ReceiveBuf[RxIdx] = SSP_ReceiveData(MDR_SSP1_BASE);
		RxIdx++;
	}
	// Сравнение отправленных и полученных данных
	TransferStatus = Verif_mem(DataSize, DataBuf, ReceiveBuf);
}

// Функция чтения полученных данных
void SPI1_Receive(uint16_t *ReceiveBuf, uint16_t DataSize)	{
	while (RxIdx < DataSize) {
		/* Чтение полученных данных */
		ReceiveBuf[RxIdx] = SSP_ReceiveData(MDR_SSP1_BASE);
		RxIdx++;
	}
}

// Функция сравнения отправленных и прочитанных данных
uint32_t Verif_mem (uint16_t BufSize, uint16_t *pBuffer1, uint16_t *pBuffer2)	{
    uint16_t i;

    for(i = 0; i < BufSize; i++)	{
        if (*pBuffer1++ != *pBuffer2++)	{
        	  return(0);
        }
    }
    return(1);
}
