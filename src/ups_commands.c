///************************************************************************************//**
//* \file         src/ups_commands.c
//* \brief        Power_sys application module file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//
//#include <string.h>
//#include <stdio.h>
//#include <ctype.h>
//#include "FreeRTOS.h"
//#include "portmacro.h"
//#include "task.h"
//
//#include "types.h"
//#include "misc.h"
//#include "adc.h"
//#include "mp2731.h"
//#include "uart.h"
//#include "trace.h"
//#include "ups_commands.h"
//
///****************************************************************************************
//* Global Definitions
//****************************************************************************************/
//#define UPS_DEBUG 0
//
//MODULE_NAME("ups_commands.c");
//
//typedef int (*ups_answer_cbk_t)(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t buffer_out_len);
//
//typedef struct {
//    const char *cmd;
//    uint32_t cmd_len;
//    ups_answer_cbk_t cmd_cbk;
//} ups_commands_t;
//
//typedef struct {
//    float v_in;
//    float v_fault;
//    float v_out;
//    uint32_t i_percent;
//    float freq;
//    float battery;
//    float temp;
//    ups_status_t status;
//    int shutdown_delay;
//    int restore_delay;
//    int test_time_sec;
//    volatile uint8_t cmd;
//} ups_t;
//
//static ups_t ups = {
//        .v_in = 222.0,
//        .v_fault = 140.0,
//        .v_out = 220.0,
//        .i_percent = 50,
//        .battery = 24.0,
//        .temp = 19.3,
//        .status = 0b00000000,
//        .freq = 0,
//        .shutdown_delay = 0,
//        .restore_delay = 0,
//        .cmd = 0,
//        .test_time_sec = 0,
//};
//
//static int ups_prepare_and_send_status(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_test_bat_10_sec(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_test_bat_until_low(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_test_bat_time(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_toggle_beep(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_shutdown(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_shutdown_and_restore(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_shutdown_cansel(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_cansel_all_tests(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_information(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len);
//static int ups_prepare_ups_rating(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) ;
//static int ups_prepare_ups_accum_rating(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) ;
//
//static ups_commands_t ups_commands[] = {
//        {
//                .cmd = "Q1<cr>",
//                .cmd_len = 2,
//                .cmd_cbk = ups_prepare_and_send_status
//        },
//        {
//                .cmd = "T<cr>",
//                .cmd_len = 1,
//                .cmd_cbk = ups_prepare_test_bat_10_sec
//        },
//        {
//                .cmd = "TL<cr>",
//                .cmd_len = 2,
//                .cmd_cbk = ups_prepare_test_bat_until_low
//        },
//        {
//                .cmd = "T<n><cr>",
//                .cmd_len = 3,
//                .cmd_cbk = ups_prepare_test_bat_time
//        },
//        {
//                .cmd = "Q<cr>",
//                .cmd_len = 1,
//                .cmd_cbk = ups_prepare_toggle_beep
//        },
//        {
//                .cmd = "S<n><cr>",
//                .cmd_len = 3,
//                .cmd_cbk = ups_prepare_shutdown
//        },
//        {
//                .cmd = "S<n>R<m><cr>",
//                .cmd_len = 8,
//                .cmd_cbk = ups_prepare_shutdown_and_restore
//        },
//        {
//                .cmd = "C<cr>",
//                .cmd_len = 1,
//                .cmd_cbk = ups_prepare_shutdown_cansel
//        },
//        {
//                .cmd = "CT<cr>",
//                .cmd_len = 2,
//                .cmd_cbk = ups_prepare_cansel_all_tests
//        },
//        {
//                .cmd = "I<cr>",
//                .cmd_len = 1,
//                .cmd_cbk = ups_prepare_information
//        },
//        {
//                .cmd = "F<cr>",
//                .cmd_len = 1,
//                .cmd_cbk = ups_prepare_ups_rating
//        },
//        {
//                .cmd = "QBV<cr>",
//                .cmd_len = 3,
//                .cmd_cbk = ups_prepare_ups_accum_rating
//        },
//};
//
//void ups_set_status(ups_status_t set_status)
//{
//    ups.status |= set_status;
//}
//
//void ups_reset_status(ups_status_t reset_status)
//{
//    ups.status &= ~reset_status;
//}
//
//ups_host_commands_t ups_get_host_cmd()
//{
//    return ups.cmd;
//}
//
//void ups_reset_host_cmd(ups_host_commands_t cmd)
//{
//    ups.cmd &= ~cmd;
//}
//
//int ups_get_shutdown_delay()
//{
//    return ups.shutdown_delay;
//}
//
//int ups_get_restore_delay()
//{
//    return ups.restore_delay;
//}
//
//int ups_get_test_time_sec()
//{
//    return ups.test_time_sec;
//}
//
//static int ups_prepare_and_send_status(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    if (in_len != 2) {
//        goto out;
//    }
//    if (strncmp("Q1", buffer_in, 2) != 0) {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    /*
//     * > [D\r]
//     * < [(226.0 195.0 226.0 014 49.0 27.5 30.0 00001000\r]
//     *    01234567890123456789012345678901234567890123456
//     *    0         1         2         3         4
//     */
//    result = snprintf(buffer_out, out_max_len,
//                      "(%03d.%01d %03d.%01d %03d.%01d %03u %02d.%01d %02d.%01d %02d.%01d " BYTE_TO_BINARY_PATTERN "\r",
//                      GET_INT_AND_FRAG(ups.v_in, 10),
//                      GET_INT_AND_FRAG(ups.v_fault, 10),
//                      GET_INT_AND_FRAG(ups.v_out, 10),
//                      (uint32_t) ups.i_percent,
//                      GET_INT_AND_FRAG(ups.freq, 10),
//                      GET_INT_AND_FRAG(ups.battery, 10),
//                      GET_INT_AND_FRAG(ups.temp, 10),
//                      BYTE_TO_BINARY(ups.status));
//    TRACE_INFO("UPS: Q1 stat: ");
//    out:
//    return result;
//}
//
//static int ups_prepare_test_bat_10_sec(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    if (in_len != 1) {
//        goto out;
//    }
//    if (strncmp("T", buffer_in, 1) != 0) {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    //ups.status |= UPS_TEST_ON;
//
//    TRACE_INFO("UPS: T test: ");
//
//    ups.test_time_sec = 10;
//    ups.cmd |= CMD_TEST;
//
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "test battery 10 seconds\r");
//#endif // UPS_DEBUG
//    out:
//    return result;
//}
//
//static int ups_prepare_test_bat_until_low(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    if (in_len != 2) {
//        goto out;
//    }
//    if (strncmp("TL", buffer_in, 2) != 0) {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    //ups.status |= UPS_TEST_ON;
//
//    TRACE_INFO("UPS: T test ulow: ");
//
//    ups.test_time_sec = -1;
//    ups.cmd |= CMD_TEST;
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "test battery until low\r");
//#endif // UPS_DEBUG
//    out:
//    return result;
//}
//
//static int ups_prepare_test_bat_time(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    uint32_t time_test = 0;
//
//    if (in_len != 3) {
//        goto out;
//    }
//    if (strncmp("T", buffer_in, 1) != 0) {
//        goto out;
//    }
//    if (!isdigit((int) (buffer_in[1])) || !isdigit((int) (buffer_in[2]))) {
//        goto out;
//    }
//    time_test = (buffer_in[1] - 0x30) * 10 + (buffer_in[2] - 0x30);
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    //ups.status |= UPS_TEST_ON;
//
//    TRACE_INFO("UPS: T time: %d", time_test);
//
//    ups.test_time_sec = time_test;
//    ups.cmd |= CMD_TEST;
//
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "test battery for %d minutes\r", time_test);
//#endif // UPS_DEBUG
//    out:
//    return result;
//}
//
//static int ups_prepare_toggle_beep(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//
//    if (in_len != 1) {
//        goto out;
//    }
//    if (buffer_in[0] != 'Q') {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    if ((ups.status & UPS_BEEP_ON) == UPS_BEEP_ON) {
//        ups.status &= ~UPS_BEEP_ON;
//    } else {
//        ups.status |= UPS_BEEP_ON;
//    }
//    TRACE_INFO("UPS: Beep toggle");
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "ups beeper toggle\r");
//#endif // UPS_DEBUG
//    out:
//    return result;
//}
//
//static uint32_t get_values_from_dot_format(char sym1, char sym2) {
//    int result = -1;
//    if (isdigit(sym1)) {
//        result = (sym1 - 0x30) * 10 + (sym2 - 0x30);
//        result *= 60;
//    } else if (sym1 == '.') {
//        result = (sym2 - 0x30);
//        result *= 6;
//    }
//    return result;
//}
//
//static int ups_prepare_shutdown(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    uint32_t delay;
//
//    if (in_len != 3) {
//        goto out;
//    }
//    if (buffer_in[0] != 'S') {
//        goto out;
//    }
//    if (!(isdigit(buffer_in[1])
//          || buffer_in[1] == '.')
//        || !isdigit(buffer_in[2])) {
//        goto out;
//    }
//    delay = get_values_from_dot_format(buffer_in[1], buffer_in[2]);
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//
//    TRACE_INFO("UPS: Shutdown time: %d", delay);
//
//    ups.shutdown_delay = delay;
//    ups.cmd |= CMD_SHUTDOWN;
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "ups shutdown after %d sec\r", delay);
//#endif // UPS_DEBUG
//    out:
//    return result;
//
//}
//
//static int ups_prepare_shutdown_and_restore(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    uint32_t shutdown_delay = 0;
//    uint32_t restore_delay = 0;
//
//    if (in_len != 8) {
//        goto out;
//    }
//    if (buffer_in[0] != 'S' || buffer_in[3] != 'R') {
//        goto out;
//    }
//    if (!(isdigit(buffer_in[1])
//          || buffer_in[1] == '.')
//        || !isdigit(buffer_in[2])) {
//        goto out;
//    }
//    if (!isdigit(buffer_in[4])
//        || !isdigit(buffer_in[5])
//        || !isdigit(buffer_in[6])
//        || !isdigit(buffer_in[7])) {
//        goto out;
//    }
//    shutdown_delay = get_values_from_dot_format(buffer_in[1], buffer_in[2]);
//    restore_delay = (buffer_in[4] - 0x30) * 1000
//                    + (buffer_in[5] - 0x30) * 100
//                    + (buffer_in[6] - 0x30) * 10
//                    + (buffer_in[7] - 0x30);
//    restore_delay *= 60;
//
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//
//    TRACE_INFO("UPS: SR Shutdwn tme: %d rest: %d", shutdown_delay, restore_delay);
//
//    ups.shutdown_delay = shutdown_delay;
//    ups.restore_delay  = restore_delay;
//    ups.cmd |= (CMD_SHUTDOWN | CMD_RESTORE);
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "ups off after %d sec and on after %d min\r",
//                      shutdown_delay, restore_delay / 60);
//#endif // UPS_DEBUG
//    out:
//    return result;
//
//}
//
//static int ups_prepare_shutdown_cansel(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//
//    if (in_len != 1) {
//        goto out;
//    }
//    if (buffer_in[0] != 'C') {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    TRACE_INFO("UPS: C Shutdwn cancel");
//
//    ups.restore_delay  = 0;
//    ups.cmd |= ( CMD_SHUTDOWN_CANSEL);
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "shutdown cansel\r");
//#endif // UPS_DEBUG
//
//    out:
//    return result;
//
//}
//
//static int ups_prepare_cansel_all_tests(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//
//    if (in_len != 2) {
//        goto out;
//    }
//    if (buffer_in[0] != 'C' &&
//        buffer_in[1] != 'T') {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    //ups.status &= ~UPS_TEST_ON;
//    ups.cmd |= ( CMD_TEST_CANSEL);
//    TRACE_INFO("UPS: CT cancel all tests");
//#if (UPS_DEBUG == 1)
//    result = snprintf(buffer_out, out_max_len, "cansel all tests\r");
//#endif // UPS_DEBUG
//
//    out:
//    return result;
//
//}
//
//static int ups_prepare_information(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//
//    if (in_len != 1) {
//        goto out;
//    }
//    if (buffer_in[0] != 'I') {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = snprintf(buffer_out, out_max_len, "#B4COMTECH       UPS_X      01.00.00  \r");
//    /*
//     *                                       < [#-------------   ------     VT12046Q  \r]
//     *                                          012345678901234567890123456789012345678
//     *                                          0         1         2         3
//     */
//
//    TRACE_INFO("UPS: I info");
//    out:
//    return result;
//}
//
//
//static int ups_prepare_ups_rating(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    if (in_len != 1) {
//        goto out;
//    }
//    if (buffer_in[0] != 'F') {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    /*
//     * > [F\r]
//     * < [#220.0 000 024.0 50.0\r]
//     *    0123456789012345678901
//     *    0         1         2
//     */
//    result = snprintf(buffer_out, out_max_len,
//                      "#012.0 002 004.0 00.0\r");
//    TRACE_INFO("UPS: F rating");
//    out:
//    return result;
//}
//
//static int ups_prepare_ups_accum_rating(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    int result = -1;
//    if (in_len != 3) {
//        goto out;
//    }
//    if (strncmp("QBV", buffer_in,3)) {
//        goto out;
//    }
//    memset(buffer_out, 0, out_max_len);
//    result = 0;
//    /*
//     * > [QBV\r]
//     * < [#024.0 04 02 100 015\r]
//     *    012345678901234567890
//     *    0         1         2
//     */
//    result = snprintf(buffer_out, out_max_len,
//                      "#%03d.%01d 01 00 100 015\r",
//                      GET_INT_AND_FRAG(ups.battery, 10));
//    TRACE_INFO("UPS: QVB acc rating");
//    out:
//    return result;
//}
//
//static voltages_t voltages;
//static mp_values_t mp_values;
//
//inline static int ups_update_values(void)
//{
//    if (adc_get_voltages(&voltages) == 0) {
//        ups.v_in = voltages.input_voltage;
//        ups.v_fault = 10.5;
//        ups.v_out = voltages.load_voltage;
//        ups.battery = voltages.batt_voltage;
//    }
//    if (mp_get_values(&mp_values) == 0) {
//        ups.temp = mp_values.v_ntc;
//    }
//    return 0;
//}
//
//inline static int ups_cmd_handler(char *buffer_in, uint32_t in_len, char *buffer_out, uint32_t out_max_len) {
//    uint32_t i;
//    int result = 0;
//
//    portENTER_CRITICAL();
//
//    for (i = 0; i < COUNT_OF(ups_commands); ++i) {
//        if (in_len >= ups_commands[i].cmd_len) {
//            if ((result = ups_commands[i].cmd_cbk(buffer_in, in_len, buffer_out, out_max_len)) >= 0) {
//                break;
//            }
//        }
//    }
//    portEXIT_CRITICAL();
//    if (result < 0) {
//        // command handle error
//        result = 0;
//    }
//    return result;
//}
//
//static unsigned int in_len = 0;
//static int out_len = 0;
//static char ups_buffer[256];
//static unsigned int cto_len = 0;
//
//void (*pProgResetHandler)(void);
//
//#define CPU_BOOTLOADER_PTR 0x00000004
//
//static void ups_work_task(void *parameters)
//{
//    parameters = parameters;
//
//    char in_char;
//    int i = 100;
//    while (1) {
//        if (++i >= 100) {
//            ups_update_values();
//            i = 0;
//        }
//        if (uart_get_byte(&in_char)) {
//            if(in_char =='\r' || in_char == '\n') {
//                if(in_len) {
//                    TRACE_DEBUG("%s", ups_buffer);
//                    if ((out_len =
//                            ups_cmd_handler(ups_buffer, in_len, ups_buffer, sizeof(ups_buffer))) != 0) {
//                        uart_send_buf(ups_buffer, out_len);
//                    }
//                    memset(ups_buffer, 0, sizeof (ups_buffer));
//                    //uart_SendBuf("\r\n", 2);
//                    in_len = out_len = 0;
//                    cto_len = 0;
//                    vTaskDelay(100);
//                 }
//            } else if( in_char == 0xff) {
//                TRACE_INFO("XCP connect");
//                cto_len++;
//                if (cto_len >= 3) {
//                        // Got boot loader
//                        TRACE_WARNING("Got bootloader");
//
//                        portENTER_CRITICAL();
//                        for(i=0; i<8; i++){
//                            NVIC->ICER[i] = 0xffffffff; // disable interrupts
//                            NVIC->ICPR[i] = 0xffffffff;
//                        }
//                        SCB->VTOR = 0x08000000 & (uint32_t)0x1fffff80;
//                        pProgResetHandler = (void(*)(void))(*((uint32_t *)CPU_BOOTLOADER_PTR));
//                        portEXIT_CRITICAL();
//                        __set_CONTROL(0);
//                        // Jump to bootloader
//                        pProgResetHandler();
//
//                }
//            } else {
//                if (in_len < sizeof (ups_buffer) - 2) {
//                     ups_buffer[in_len++] = in_char;
//                    vTaskDelay(1);
//                } else {
//                    memset(ups_buffer, 0, sizeof (ups_buffer));
//                    in_len = 0;
//                }
//            }
//        } else {
//            vTaskDelay(10);
//        }
//    }
//}
//
//int ups_commands_init(void)
//{
//    xTaskCreate(ups_work_task, "ups", configMINIMAL_STACK_SIZE * 4,
//            NULL, configMAX_PRIORITIES - 1, NULL);
//    return 0;
//}
//
///*********************************** end of ups_commands.c *****************************/
