/*
 *
 *   Utility functions
 *
 */
	.syntax	unified
	.cpu cortex-m3

	.text
	.thumb
	.thumb_func
	.align	2
	.globl	led_on_asm
	.type	led_on_asm, %function

	led_on_asm:
	/* r0 - port base, r1 - pin mask */
	ldr     r2, [r0]
	orr     r1, r2
	str     r1, [r0]
	bx      lr

	.thumb
	.thumb_func
	.align	2
	.globl	led_off_asm
	.type	led_off_asm, %function

	led_off_asm:
	/* r0 - port base, r1 - pin mask */
	ldr     r2, [r0]
	bic     r2, r1
	str     r2, [r0]
	bx      lr

	.thumb
	.thumb_func
	.align	2
	.globl	delay_asm
	.type	delay_asm, %function

	delay_asm:
	/* r0 - delay count */
	loop_delay_asm:
	nop
	subs    r0, 1
	bne     loop_delay_asm
	bx      lr

	.thumb
	.thumb_func
	.align	2
	.globl	init_port_asm
	.type	init_port_asm, %function

	init_port_asm:
	/* r0 - ptr to port settings */

	bx lr

    .end
