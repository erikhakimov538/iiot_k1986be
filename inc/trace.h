/************************************************************************************//**
* \file         inc/trace.h
* \brief        Power_sys application header file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

#ifndef TRACE_H_
#define TRACE_H_

/****************************************************************************************
* Include files
****************************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "FreeRTOS.h"

/****************************************************************************************
* Global Definitions
****************************************************************************************/
#define BYTE_TO_BINARY_PATTERN "%c%c%c%c%c%c%c%c"
#define BYTE_TO_BINARY(byte)  \
  (byte & 0x80 ? '1' : '0'), \
  (byte & 0x40 ? '1' : '0'), \
  (byte & 0x20 ? '1' : '0'), \
  (byte & 0x10 ? '1' : '0'), \
  (byte & 0x08 ? '1' : '0'), \
  (byte & 0x04 ? '1' : '0'), \
  (byte & 0x02 ? '1' : '0'), \
  (byte & 0x01 ? '1' : '0')

#define GET_INT_AND_FRAG(x, prec)   \
    (uint32_t)x,                    \
    (uint32_t)((uint32_t)(x*prec)%prec)


#define TRACE_LEVEL_DEBUG      5
#define TRACE_LEVEL_INFO       4
#define TRACE_LEVEL_WARNING    3
#define TRACE_LEVEL_ERROR      2
#define TRACE_LEVEL_FATAL      1
#define TRACE_LEVEL_NO_TRACE   0


#define TRACE_MAX_BUF_LEN   128

#define MODULE_NAME(X) static const char* _modulename_ = #X
#define THIS_MODULE        _modulename_

// By default, all traces are output except the debug one.
#if !defined(TRACE_LEVEL)
#define TRACE_LEVEL TRACE_LEVEL_INFO
#endif

// By default, trace level is static (not dynamic)
#if !defined(DYN_TRACES)
#define DYN_TRACES 0
#endif

#if defined(NOTRACE)
#error "Error: NOTRACE has to be not defined !"
#endif

#undef NOTRACE
#if (DYN_TRACES==0)
    #if (TRACE_LEVEL == TRACE_LEVEL_NO_TRACE)
        #define NOTRACE
    #endif
#endif

/************************************************************************************//**
** \brief     Lock trace for tasks.
** \return    none.
**
****************************************************************************************/
inline static void trace_lock(void)
{
    portENTER_CRITICAL();
}
/************************************************************************************//**
** \brief     UnLock trace for tasks.
** \return    none.
**
****************************************************************************************/
inline static void trace_unlock(void)
{
    portEXIT_CRITICAL();
}

void trace_printf(const char* format, ...);


#if defined(NOTRACE)

// Empty macro
#define TRACE_DEBUG(...)      { }
#define TRACE_INFO(...)       { }
#define TRACE_WARNING(...)    { }
#define TRACE_ERROR(...)      { }
#define TRACE_FATAL(...)      { while(1); }
#define TRACE_PERM(...)       { }

#define TRACE_DEBUG_WP(...)   { }
#define TRACE_INFO_WP(...)    { }
#define TRACE_WARNING_WP(...) { }
#define TRACE_ERROR_WP(...)   { }
#define TRACE_FATAL_WP(...)   { while(1); }
#define TRACE_PERM_WP(...)    { }

#elif (DYN_TRACES == 1)

// Trace output depends on traceLevel value
#define TRACE_DEBUG(...)      { if (traceLevel >= TRACE_LEVEL_DEBUG)   { trace_printf("-D- " __VA_ARGS__); } }
#define TRACE_INFO(...)       { if (traceLevel >= TRACE_LEVEL_INFO)    { trace_printf("-I- " __VA_ARGS__); } }
#define TRACE_WARNING(...)    { if (traceLevel >= TRACE_LEVEL_WARNING) { trace_printf("-W- " __VA_ARGS__); } }
#define TRACE_ERROR(...)      { if (traceLevel >= TRACE_LEVEL_ERROR)   { trace_printf("-E- " __VA_ARGS__); } }
#define TRACE_FATAL(...)      { if (traceLevel >= TRACE_LEVEL_FATAL)   { trace_printf("-F- " __VA_ARGS__); while(1); } }
#define TRACE_PERM(...)       { trace_printf("-P- " __VA_ARGS__); }

#define TRACE_DEBUG_WP(...)   { if (traceLevel >= TRACE_LEVEL_DEBUG)   { trace_printf(__VA_ARGS__); } }
#define TRACE_INFO_WP(...)    { if (traceLevel >= TRACE_LEVEL_INFO)    { trace_printf(__VA_ARGS__); } }
#define TRACE_WARNING_WP(...) { if (traceLevel >= TRACE_LEVEL_WARNING) { trace_printf(__VA_ARGS__); } }
#define TRACE_ERROR_WP(...)   { if (traceLevel >= TRACE_LEVEL_ERROR)   { trace_printf(__VA_ARGS__); } }
#define TRACE_FATAL_WP(...)   { if (traceLevel >= TRACE_LEVEL_FATAL)   { trace_printf(__VA_ARGS__); while(1); } }
#define TRACE_PERM_WP(...)    { trace_printf(__VA_ARGS__); }

#else

// Trace compilation depends on TRACE_LEVEL value
#if (TRACE_LEVEL >= TRACE_LEVEL_DEBUG)
#define TRACE_DEBUG(...)      {trace_lock(); trace_printf("-D- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_DEBUG_WP(...)   {trace_lock(); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#else
#define TRACE_DEBUG(...)      { }
#define TRACE_DEBUG_WP(...)   { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_INFO)
#define TRACE_INFO(...)       {trace_lock(); trace_printf("-I- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_INFO_WP(...)    {trace_lock(); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#else
#define TRACE_INFO(...)       { }
#define TRACE_INFO_WP(...)    { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_WARNING)
#define TRACE_WARNING(...)    {trace_lock(); trace_printf("-W- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_WARNING_WP(...) {trace_lock(); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#else
#define TRACE_WARNING(...)    { }
#define TRACE_WARNING_WP(...) { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_ERROR)
#define TRACE_ERROR(...)      {trace_lock(); trace_printf("-E- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_ERROR_WP(...)   {trace_lock(); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#else
#define TRACE_ERROR(...)      { }
#define TRACE_ERROR_WP(...)   { }
#endif

#if (TRACE_LEVEL >= TRACE_LEVEL_FATAL)
#define TRACE_FATAL(...)      {trace_lock(); trace_printf("-F- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n"); while(1);trace_unlock(); }
#define TRACE_FATAL_WP(...)   {trace_lock(); trace_printf("" __VA_ARGS__);trace_printf("\r\n"); while(1);trace_unlock(); }
#else
#define TRACE_FATAL(...)      { while(1); }
#define TRACE_FATAL_WP(...)   { while(1); }
#endif

#define TRACE_PERM(...)       {trace_lock(); trace_printf("-P- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_PERM_WP(...)    {trace_lock(); trace_printf("" __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }

#endif

#ifdef TEST_ENA

#define TRACE_TEST(...)       {trace_lock(); trace_printf("-TEST- %s:%i ", THIS_MODULE, __LINE__); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_TEST_WP(...)    {trace_lock(); trace_printf("-TEST-"); trace_printf(" " __VA_ARGS__);trace_printf("\r\n");trace_unlock(); }
#define TRACE_TEST_SLNT(...)  {trace_lock(); trace_printf(" " __VA_ARGS__);trace_unlock(); }
#else
#define TRACE_TEST(...)       { }
#define TRACE_TEST_WP(...)    { }
#define TRACE_TEST_SLNT(...)  { }

#endif

/****************************************************************************************
* Exported variables
****************************************************************************************/
#if !defined(NOTRACE) && (DYN_TRACES == 1)
    extern unsigned int traceLevel;
#endif


#endif //#ifndef TRACE_H_

/*********************************** end of trace.h **************************************/
