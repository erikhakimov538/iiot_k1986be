/************************************************************************************//**
* \file         inc/pins.h
* \brief        Power_sys application header file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#ifndef PINS_H_
#define PINS_H_

#include <stdint.h>

typedef enum {
    PIN_TRANSPORT,
    PIN_DC_IN_CONTROL,
    PIN_BACKUP_CONTROL,
    PIN_CHARGE_EN,
    PIN_BAT_TEST,
    PIN_STATUS_0,
    PIN_STATUS_1,
    PIN_MAX,
} pins_num_t;

int pin_set(pins_num_t pin);
int pin_reset(pins_num_t pin);
int pin_get(pins_num_t pin);

#endif // PINS_H_

/*********************************** end of pins.h **************************************/
