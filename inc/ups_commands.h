/************************************************************************************//**
* \file         inc/ups_commands.h
* \brief        Power_sys application header file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/

#ifndef POWER_SYS_UPS_COMMANDS_H
#define POWER_SYS_UPS_COMMANDS_H

typedef enum ups_status  {
    UPS_BEEP_ON     = 0x1 << 0,
    UPS_OFF         = 0x1 << 1,
    UPS_TEST_ON     = 0x1 << 2,
    UPS_LINEER_UPS  = 0x1 << 3,
    UPS_FAILED      = 0x1 << 4,
    UPS_BYPASS      = 0x1 << 5,
    UPS_BATT_LOW    = 0x1 << 6,
    UPS_BATT_WORK   = 0x1 << 7

} ups_status_t;

typedef enum {
    CMD_SHUTDOWN    = 0x1 << 0,
    CMD_RESTORE     = 0x1 << 1,
    CMD_TEST        = 0x1 << 2,
    CMD_TEST_CANSEL = 0x1 << 3,
    CMD_SHUTDOWN_CANSEL    = 0x1 << 4,
} ups_host_commands_t;


int ups_commands_init(void);
void ups_set_status(ups_status_t set_status);
void ups_reset_status(ups_status_t reset_status);
ups_host_commands_t ups_get_host_cmd();
void ups_reset_host_cmd(ups_host_commands_t cmd);
int ups_get_shutdown_delay();
int ups_get_restore_delay();
int ups_get_test_time_sec();

#endif //POWER_SYS_UPS_COMMANDS_H
/*********************************** end of ups_commands.h *****************************/
