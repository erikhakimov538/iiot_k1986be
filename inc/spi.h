#ifndef __SPI_H__
#define __SPI_H__

#include "stdint.h"
#include "misc.h"

void SPI_Init (void);
void SPI1_Transmit(uint16_t *DataBuf, uint16_t DataSize);
void SPI1_TransmitReceive(uint16_t *DataBuf, uint16_t *ReceiveBuf, uint16_t DataSize);
void SPI1_Receive(uint16_t *ReceiveBuf, uint16_t DataSize);
uint32_t Verif_mem (uint16_t BufSize, uint16_t *pBuffer1, uint16_t *pBuffer2);

#endif  /* __SPI_H__ */
