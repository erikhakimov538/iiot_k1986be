/************************************************************************************//**
* \file         inc/uart.h
* \brief        Power_sys application header file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

/****************************************************************************************
* Include files
****************************************************************************************/
#ifndef __UART_H__
#define __UART_H__

#include "stdint.h"
#include "misc.h"


void uart_init(unsigned int speed, unsigned short bit_cnt, 
                unsigned short stop_bits, unsigned short parity);
bool uart_send_byte(char byte);
bool uart_get_byte(char* pb);
bool uart_send_str(char* str);
bool uart_send_buf(char* buf, unsigned int size);
void uart_irq(void);
uint32_t uart_get_received_count(void);


#endif  /* __UART_H__ */

/*********************************** end of uart.h **************************************/
