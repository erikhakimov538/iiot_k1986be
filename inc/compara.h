///************************************************************************************//**
//* \file         inc/compara.h
//* \brief        Power_sys application header file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
//#ifndef POWERSYS_COMPARA_H_
//#define POWERSYS_COMPARA_H_
//
//#include "stdint.h"
//
//typedef enum {
//    COMP_LVL_STANDARD,
//    COMP_LVL_DOWN,
//} compara_levels_t;
//
//int compara_init(void);
//uint32_t compara_get_untriggered_status(void) ;
//int compara_down_level(void);
//int compara_up_level(void);
//int compara_is_lvl_standard(void);
//
//#endif // POWERSYS_COMPARA_H_
//
//
//
///*********************************** end of compara.h **************************************/
//
//
