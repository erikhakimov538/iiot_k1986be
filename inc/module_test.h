///************************************************************************************//**
//* \file         inc/module_test.h
//* \brief        Power_sys application header file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//#ifndef MODULE_TEST_H_
//#define MODULE_TEST_H_
//
///****************************************************************************************
//* Definitions
//****************************************************************************************/
//#define TEST_OK     (1)
//#define TEST_FAIL   (0)
//
//typedef enum {
//    TST_NONE = 0,
//} test_check_list_t;
//
///****************************************************************************************
//* Function definitions
//****************************************************************************************/
//int  tests_init(void);
//void test_print_test_result(void);
//
//#endif /* MODULE_TEST_H_ */
///*********************************** end of module_test.h **************************************/
