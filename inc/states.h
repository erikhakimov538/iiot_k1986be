/************************************************************************************//**
* \file         inc/states.h
* \brief        Power_sys application header file.
* \ingroup      Power_sys__
* \internal
*----------------------------------------------------------------------------------------
*                          C O P Y R I G H T
*----------------------------------------------------------------------------------------
*
* Copyright (c) 2022 B4comtech software     https://b4com.tech
*
*----------------------------------------------------------------------------------------
*                            L I C E N S E
*----------------------------------------------------------------------------------------
* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
*
* \endinternal
****************************************************************************************/

#ifndef STATES_H_
#define STATES_H_

#define DC_IN_MAX   (15.0)
#define DC_IN_MIN   (10.0)

#define DC_LOAD_MAX (14.0)
#define DC_LOAD_MIN (10.5)

#define DC_BACKUP_MIN (11.0)
#define DC_BACKUP_MAX (11.9)

#define VBAT_MIN    (3.4)

#define MONITOR_SLEEP (100)
#define MONITOR_TIMEOUT (2000/MONITOR_SLEEP)
#define DC_LOAD_TIMEOUT (2000)
#define ADC_RETRIES (10)

/****************************************************************************************
* Function prototypes
****************************************************************************************/
int states_init();
const char* states_get_cur_name();

#endif /* STATES_H_ */
/*********************************** end of states.h **************************************/
