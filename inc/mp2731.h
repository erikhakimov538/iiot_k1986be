///************************************************************************************//**
//* \file         inc/mp2731.h
//* \brief        Power_sys application header file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
///****************************************************************************************
//* Include files
//****************************************************************************************/
//#ifndef MP_2731_H_
//#define MP_2731_H_
//
//#include <stdint.h>
//
//typedef enum {
//    V_BAT_ERR           = 0x1 << 0,
//    V_IN_ERR            = 0x1 << 1,
//    V_SYS_ERR           = 0x1 << 2,
//    V_NTC_ERR           = 0x1 << 3,
//    V_CURR_CHRG_ERR     = 0x1 << 4,
//    V_CURR_IN_LOCAL_ERR = 0x1 << 5,
//} mp_values_status_t;
//
//typedef struct {
//    float v_bat;
//    float v_in;
//    float v_sys;
//    float v_ntc;
//    float curr_chrg;
//    float curr_in_local;
//    uint8_t status;
//} mp_values_t;
//
//typedef enum {
//    EPARM   = -1,
//    ELOCK   = -2,
//    EVALUE  = -3,
//    EUNINIT = -4
//} mp_status_errors_t;
//
//int mp_i2c_init();
//mp_status_errors_t mp_get_values(mp_values_t* v);
//int mp_enable_adc_vbat(void);
//int mp_disable_batfet(void);
//int mp_enable_batfet(void);
//int mp_enable_dcdc(void);
//int mp_disable_dcdc(void);
//
//#endif // MP_2731_H_
//
///*********************************** end of mp2731.h **************************************/
