///************************************************************************************//**
//* \file         inc/adc.h
//* \brief        Power_sys application header file.
//* \ingroup      Power_sys__
//* \internal
//*----------------------------------------------------------------------------------------
//*                          C O P Y R I G H T
//*----------------------------------------------------------------------------------------
//*
//* Copyright (c) 2022 B4comtech software     https://b4com.tech
//*
//*----------------------------------------------------------------------------------------
//*                            L I C E N S E
//*----------------------------------------------------------------------------------------
//* THIS SOFTWARE HAS BEEN PROVIDED "AS IS," WITHOUT EXPRESS OR IMPLIED WARRANTY
//* INCLUDING, WITHOUT LIMITATION, IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
//* FOR A PARTICULAR USE AND NON-INFRINGEMENT.
//*
//* \endinternal
//****************************************************************************************/
//
//#ifndef POWERSYS_ADC_H_
//#define POWERSYS_ADC_H_
//
//#include "stdint.h"
//
//#define FILT_ORDER (20)
//#define CHANNELS_MAX (6)
//
///// Voltages in millivolts
//typedef struct {
//    float input_voltage;
//    float backup_voltage;
//    float load_voltage;
//    float batt_voltage;
//    float system_voltage;
//    float vref_voltage;
//    uint32_t vref_code;
//    uint32_t input_code;
//    uint32_t backup_code;
//    uint32_t batt_code;
//    uint32_t system_code;
//    uint32_t load_code;
//} voltages_t;
//
//int adc_init(void);
//int adc_get_voltages(voltages_t* v);
//
//#endif // POWERSYS_ADC_H_
//
//
//
///*********************************** end of adc.h **************************************/
//
